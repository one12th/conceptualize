package com.likeableincorporation.conceptualize;

public interface OnProfileUploadComplete {

    void onProfileUploadComplete(Boolean error);
}