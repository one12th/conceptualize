package com.likeableincorporation.conceptualize.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.likeableincorporation.conceptualize.ChatListener;
import com.likeableincorporation.conceptualize.ChatService;
import com.likeableincorporation.conceptualize.R;
import com.likeableincorporation.conceptualize.SparkerSingleton;
import com.likeableincorporation.conceptualize.repository.model.UserProfile;
import com.likeableincorporation.conceptualize.view.CircleImageView;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;

import java.util.ArrayList;

/**
 * Created by SamHarrison on 13/11/2016.
 */
public class ChatFragment extends Fragment implements ChatListener {

    private LayoutInflater mInflater;
    private ListView listView;
    public QBChatDialog dialog;
    private EditText messageField;
    private Button sendButton;
    private CircleImageView profileImageView;
    private TextView titleTextView;
    private UserProfile dialogOpponent;
    private TextView connectingHeader;

    public ChatFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mInflater = inflater;

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat, container, false);

        ImageButton backButton = view.findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                getFragmentManager().popBackStack();
            }
        });

        profileImageView = view.findViewById(R.id.profile_image_view);
        profileImageView.setImageResource(R.drawable.placeholder);

        dialogOpponent = ChatService.getInstance().getDialogOpponent(dialog);

        profileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileFragment fragment = new ProfileFragment();
                Bundle args = new Bundle();
                args.putBoolean("cards", false);
                args.putInt("index", ChatService.getInstance().userProfiles.indexOf(dialogOpponent));
                fragment.setArguments(args);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.layout, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        titleTextView = view.findViewById(R.id.chat_title_text);
        if (dialogOpponent != null && dialogOpponent.name != null) {
            titleTextView.setText(dialogOpponent.name);
        }

        listView = view.findViewById(R.id.chat_list_view);
        listView.setAdapter(mListAdapter);

        messageField = view.findViewById(R.id.message_text_field);

        sendButton = view.findViewById(R.id.send_message_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = messageField.getText().toString();
                if (message.equals("")) {
                    return;
                }
                ChatService.getInstance().sendMessage(message, dialog);
                messageField.setText("");
            }
        });

        connectingHeader = view.findViewById(R.id.connecting_header);

        if (QBChatService.getInstance().isLoggedIn()) {
            connectingHeader.setVisibility(View.GONE);
        } else {
            connectingHeader.setVisibility(View.VISIBLE);
        }

        //LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver, new IntentFilter("new-message-received"));

        syncMessages();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        ChatService.getInstance().addChatListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        ChatService.getInstance().removeChatListener(this);
    }

    private void syncMessages() {
        ArrayList<QBChatMessage> messages = ChatService.getInstance().messagesForDialogID(dialog.getDialogId());

        QBRestChatService.getDialogMessages(dialog, null).performAsync(new QBEntityCallback<ArrayList<QBChatMessage>>() {
            @Override
            public void onSuccess(ArrayList<QBChatMessage> qbChatMessages, Bundle bundle) {
                if (qbChatMessages != null && qbChatMessages.size() > 0) {
                    ChatService.getInstance().addMessages(qbChatMessages);
                    mListAdapter.notifyDataSetChanged();
                }
                QBRestChatService.markMessagesAsRead(dialog.getDialogId(), null).performAsync(new QBEntityCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid, Bundle bundle) {
                        dialog.setUnreadMessageCount(0);
                        ChatService.getInstance().insertOrUpdateDialog(dialog);
                    }

                    @Override
                    public void onError(QBResponseException e) {

                    }
                });
            }

            @Override
            public void onError(QBResponseException e) {

            }
        });
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String dialogID = intent.getStringExtra("dialogID");
            if (dialogID.equals(dialog.getDialogId())) {
                QBRestChatService.markMessagesAsRead(dialog.getDialogId(), null).performAsync(new QBEntityCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid, Bundle bundle) {
                        dialog.setUnreadMessageCount(0);
                        ChatService.getInstance().insertOrUpdateDialog(dialog);
                    }

                    @Override
                    public void onError(QBResponseException e) {

                    }
                });
                mListAdapter.notifyDataSetChanged();
            }
        }
    };

    private BaseAdapter mListAdapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return ChatService.getInstance().messagesForDialogID(dialog.getDialogId()).size();
        }

        @Override
        public Object getItem(int position) {
            return ChatService.getInstance().messagesForDialogID(dialog.getDialogId()).get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView; // = convertView;
            int bubbleColor;
            QBChatMessage message = ChatService.getInstance().messagesForDialogID(dialog.getDialogId()).get(position);
            //if(rowView != null){
            if (message.getSenderId().equals(SparkerSingleton.getInstance().currentUserProfile.userID)) {
                rowView = mInflater.inflate(R.layout.right_message_item, parent, false);
                bubbleColor = R.color.mainColor;
            } else {
                rowView = mInflater.inflate(R.layout.left_message_item, parent, false);
                bubbleColor = R.color.lightGray;
            }
            //}
            TextView textView = rowView.findViewById(R.id.message_text_view);
            GradientDrawable tvBackground = (GradientDrawable) textView.getBackground();
            tvBackground.setColor(getResources().getColor(bubbleColor));
            textView.setText(message.getBody());

            return rowView;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void messageReceived(String dialogID) {
        if (dialogID.equals(dialog.getDialogId())) {
            QBRestChatService.markMessagesAsRead(dialog.getDialogId(), null).performAsync(new QBEntityCallback<Void>() {
                @Override
                public void onSuccess(Void aVoid, Bundle bundle) {
                    dialog.setUnreadMessageCount(0);
                    ChatService.getInstance().insertOrUpdateDialog(dialog);
                }

                @Override
                public void onError(QBResponseException e) {

                }
            });
            mListAdapter.notifyDataSetChanged();
        } else {
            ChatService.getInstance().displayMessageNotification(false);
        }
    }

    @Override
    public void chatConnected() {
        connectingHeader.setVisibility(View.GONE);
        sendButton.setEnabled(true);
    }

    @Override
    public void chatDisconnected() {
        connectingHeader.setVisibility(View.VISIBLE);
        sendButton.setEnabled(false);
    }
}
