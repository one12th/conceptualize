package com.likeableincorporation.conceptualize.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by SamHarrison on 17/02/2017.
 */

public class SquareImageViewH extends ImageView {

    public SquareImageViewH(Context context) {
        super(context);
    }

    public SquareImageViewH(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageViewH(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int height = getMeasuredHeight();
        setMeasuredDimension(height, height);
    }
}