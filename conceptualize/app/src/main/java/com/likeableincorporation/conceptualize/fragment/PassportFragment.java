package com.likeableincorporation.conceptualize.fragment;


import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.likeableincorporation.conceptualize.MyApplication;
import com.likeableincorporation.conceptualize.R;
import com.likeableincorporation.conceptualize.activity.MainActivity;
import com.likeableincorporation.conceptualize.async.GeocoderAsyncTask;
import com.likeableincorporation.conceptualize.repository.TinyDB;
import com.likeableincorporation.conceptualize.repository.model.UserLocation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class PassportFragment extends Fragment implements OnMapReadyCallback,
        GoogleMap.InfoWindowAdapter, GeocoderAsyncTask.Callback {

    private GoogleMap mapInstance;
    private MapView mapView;
    private Bundle mBundle;
    private Marker locationMarker;

    public PassportFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        mBundle = bundle;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_passport, container, false);

        Button closeButton = (Button) view.findViewById(R.id.close_button);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        mapView = (MapView) view.findViewById(R.id.map_view);
        mapView.onCreate(mBundle);
        mapView.getMapAsync(this);

        return view;
    }

    @Override
    public void onMapReady(GoogleMap map) {

        mapInstance = map;
        mapInstance.setInfoWindowAdapter(this);
        LatLng sydney = new LatLng(-33.867, 151.206);

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13));
        locationMarker = mapInstance.addMarker(new MarkerOptions()
                .position(mapInstance.getCameraPosition().target)
                .title("Use Location")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_location_marker)));

        locationMarker.showInfoWindow();

        mapInstance.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                Geocoder coder = new Geocoder(getContext());

                List<Address> addresses = null;
                try {
                    addresses = coder.getFromLocation(locationMarker.getPosition().latitude, locationMarker.getPosition().longitude, 1);

                    if (addresses == null || addresses.size() == 0) {
                        locationMarker.setTitle("Unknown Location");
                    } else if (addresses.get(0).getSubAdminArea() != null) {
                        locationMarker.setTitle(addresses.get(0).getSubAdminArea());
                    } else if (addresses.get(0).getAdminArea() != null) {
                        locationMarker.setTitle(addresses.get(0).getAdminArea());
                    } else {
                        locationMarker.setTitle("Unknown Location");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }


                UserLocation location = new UserLocation(locationMarker.getTitle(), locationMarker.getPosition());

                TinyDB tinydb = new TinyDB(getContext());
                ArrayList<UserLocation> locations = tinydb.getListObject("locations", UserLocation.class);
                if (locations == null) {
                    locations = new ArrayList<>();
                }
                locations.add(0, location);
                tinydb.putListObject("locations", locations);

                SharedPreferences settings = MyApplication.getAppContext().getSharedPreferences("SparkerSettings", 0);

                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("activeLocation", locations.size());
                editor.apply();

                ((MainActivity) getActivity()).swipeFragment.settingsChanged();

                getFragmentManager().popBackStack();
            }
        });

        mapInstance.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });

        mapInstance.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                locationMarker.setPosition(mapInstance.getCameraPosition().target);
            }
        });

        mapInstance.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() { }
        });

        mapView.onResume();
    }

    private void getCurrentMarkerLocation(final double lat, final double lon) {
        Double[] params = new Double[] {lat, lon};

        new GeocoderAsyncTask(getContext(), this).execute(params);
    }

    @Override
    public void onLocationFound(String message) {
        locationMarker.setTitle(message);
    }

    /* InfoWindowAdapter Implementation */

    @Override
    public View getInfoWindow(Marker marker) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.custom_info_window, null);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(marker.getTitle());
        return view;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


}
