package com.likeableincorporation.conceptualize.activity;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.security.ProviderInstaller;
import com.likeableincorporation.conceptualize.ChatService;
import com.likeableincorporation.conceptualize.MyApplication;
import com.likeableincorporation.conceptualize.R;
import com.likeableincorporation.conceptualize.SparkerSingleton;
import com.likeableincorporation.conceptualize.fragment.DialogsFragment;
import com.likeableincorporation.conceptualize.fragment.LoginFragment;
import com.likeableincorporation.conceptualize.fragment.SettingsFragment;
import com.likeableincorporation.conceptualize.fragment.SwipeFragment;
import com.likeableincorporation.conceptualize.fragment.TopicsFragment;
import com.likeableincorporation.conceptualize.view.CircleImageView;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBProvider;
import com.quickblox.auth.session.BaseService;
import com.quickblox.auth.session.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.BaseServiceException;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.SmackException;

import java.util.Date;

public class MainActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, BillingProcessor.IBillingHandler {

    public GoogleApiClient mGoogleApiClient;
    private boolean isInitialLoad = false;
    public BillingProcessor bp;
    private DrawerLayout drawerLayout;
    private LinearLayout menuDrawer;
    private RelativeLayout mainDrawer;
    public SwipeFragment swipeFragment;
    public int screenHeight;
    public int screenWidth;
    public boolean isSubscribed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MapView mv = new MapView(getApplicationContext());
                    mv.onCreate(null);
                    mv.onPause();
                    mv.onDestroy();
                } catch (Exception ignored) {

                }
            }
        }).start();

        isInitialLoad = true;

        updateAndroidSecurityProvider(this);

        bp = new BillingProcessor(this, getString(R.string.google_public_key), this);

        setContentView(R.layout.activity_main);

        swipeFragment = new SwipeFragment();

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.addDrawerListener(mDrawerListener);
        menuDrawer = (LinearLayout) findViewById(R.id.menu_drawer);
        mainDrawer = (RelativeLayout) findViewById(R.id.layout);

        LinearLayout sideMenuProfileButton = (LinearLayout) findViewById(R.id.sidemenu_profile_button);
        sideMenuProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.layout, swipeFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                swipeFragment.showOwnProfile();
                drawerLayout.closeDrawer(menuDrawer, true);
            }
        });

        LinearLayout sideMenuHomeButton = (LinearLayout) findViewById(R.id.sidemenu_home_button);
        sideMenuHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.layout, swipeFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                drawerLayout.closeDrawer(menuDrawer, true);
            }
        });

        LinearLayout sideMenuChatsButton = (LinearLayout) findViewById(R.id.sidemenu_chats_button);
        sideMenuChatsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogsFragment fragment = new DialogsFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.layout, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
                drawerLayout.closeDrawer(menuDrawer, true);
            }
        });

        LinearLayout sideMenuSettingsButton = (LinearLayout) findViewById(R.id.sidemenu_settings_button);
        sideMenuSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsFragment fragment = new SettingsFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.layout, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
                drawerLayout.closeDrawer(menuDrawer, true);
            }
        });

        if (mGoogleApiClient == null) {
            // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(AppIndex.API).build();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AccessToken.getCurrentAccessToken() == null) {
                    presentLoginScreen();
                    return;
                }
                presentSwipeScreen();
            }
        }, 500);

        LocalBroadcastManager.getInstance(this).registerReceiver(pushBroadcastReceiver, new IntentFilter("new-push-event"));

    }

    @Override
    protected void onStart() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        bp.loadOwnedPurchasesFromGoogle();
        super.onStart();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                resumeTasks();
            }
        }, 500);
    }

    private void resumeTasks() {

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(10);

        isSubscribed = bp.isSubscribed("com.samuelharrison.sparker.iap.monthly");
        if (isInitialLoad) {
            isInitialLoad = false;
            return;
        }

        if (QBChatService.getInstance().isLoggedIn()) {
            return;
        } else if (SparkerSingleton.getInstance().currentUserProfile == null) {
            //presentSwipeScreen();
            if (AccessToken.getCurrentAccessToken() == null) {
                presentLoginScreen();
            } else {
                swipeFragment.createSession();
            }
            return;
        }
        try {
            if (QBChatService.getBaseService().getTokenExpirationDate().before(new Date())) {
                recreateSession();
            } else {
                reconnectUser();
            }
        } catch (BaseServiceException e) {
            Log.d("SparkerDebug", "Error getting token, will try to reconnect anyway!");
            reconnectUser();
        }
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
        try {
            QBChatService.getInstance().logout();
        } catch (SmackException.NotConnectedException e) {
            Log.d("SparkerDebug", "Couldn't log out, no connection!");
        }
    }

    /*@Override
    protected void onPause() {
        super.onPause();

        try {
            QBChatService.getInstance().logout();
        }catch (SmackException.NotConnectedException e){
            Log.d("SparkerDebug","Couldn't log out, no connection!");
        }
    }*/

    /*@Override
    protected void onResume() {
        super.onResume();

        Fragment myFragment = getSupportFragmentManager().findFragmentByTag("LoginFragment");
        if (myFragment == null || myFragment.isVisible()) {
            return;
        }

        isSubscribed = bp.isSubscribed("com.samuelharrison.sparker.iap.monthly");

        if(QBChatService.getInstance().isLoggedIn()) {
            return;
        }else if(SparkerSingleton.getInstance().currentUserProfile == null){
            //presentSwipeScreen();
            if(AccessToken.getCurrentAccessToken() == null){
                presentLoginScreen();
            }else {
                swipeFragment.createSession();
            }
            return;
        }
        try {
            if (QBChatService.getBaseService().getTokenExpirationDate().before(new Date())){
                recreateSession();
            }else{
                reconnectUser();
            }
        }catch (BaseServiceException e){
            Log.d("SparkerDebug","Error getting token, will try to reconnect anyway!");
            reconnectUser();
        }

    }*/

    private void reconnectUser() {
        Log.d("SparkerDebug", "Will try to reconnect existing user");
        try {
            String token = BaseService.getBaseService().getToken();
            QBUser user = new QBUser();
            user.setId(SparkerSingleton.getInstance().currentUserProfile.userID);
            user.setPassword(token);
            QBChatService.getInstance().login(user, new QBEntityCallback() {
                @Override
                public void onSuccess(Object o, Bundle bundle) {
                    QBChatService.getInstance().startAutoSendPresence(60);
                    ChatService.getInstance().reInitDialogs();
                    Log.d("SparkerDebug", "Successfully logged in using existing session!");
                }

                @Override
                public void onError(QBResponseException e) {
                    logout();
                    Log.d("SparkerDebug", "Failed to login using existing session");
                }
            });
        } catch (BaseServiceException e) {
            Log.d("SparkerDebug", "Failed to get existing session token");
            recreateSession();
        }
    }

    private void recreateSession() {
        Log.d("SparkerDebug", "Will try to recreate session");

        QBUser user = new QBUser();
        user.setId(SparkerSingleton.getInstance().currentUserProfile.userID);
        user.setPassword(AccessToken.getCurrentAccessToken().getToken());
        QBAuth.createSessionUsingSocialProvider(QBProvider.FACEBOOK, AccessToken.getCurrentAccessToken().getToken(), null).performAsync(new QBEntityCallback<QBSession>() {
            @Override
            public void onSuccess(QBSession qbSession, Bundle bundle) {
                Log.d("SparkerDebug", "Created new session, will try reconnecting user");
                reconnectUser();
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("SparkerDebug", "Error creating new session!");
                logout();
            }
        });

        /*QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
        requestBuilder.eq("user_id",SparkerSingleton.getInstance().currentUserProfile.userID);
        QBCustomObjects.getObjects("profile", requestBuilder).performAsync(new QBEntityCallback<ArrayList<QBCustomObject>>() {
            @Override
            public void onSuccess(ArrayList<QBCustomObject> qbCustomObjects, Bundle bundle) {
                Log.d("SparkerDebug","Session should now be refreshed!");
            }
            @Override
            public void onError(QBResponseException e) {
                Log.d("SparkerDebug","Error trying to refresh session!");
                Log.d("SparkerDebug",e.getLocalizedMessage());
            }
        });*/
    }

    public void toggleMenu() {
        if (drawerLayout.isDrawerOpen(menuDrawer)) {
            drawerLayout.closeDrawer(menuDrawer, true);
        } else {
            drawerLayout.openDrawer(menuDrawer, true);
        }
    }

    private DrawerLayout.DrawerListener mDrawerListener = new DrawerLayout.DrawerListener() {
        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {
            mainDrawer.setX(drawerView.getWidth() * slideOffset);
        }

        @Override
        public void onDrawerOpened(View drawerView) {

        }

        @Override
        public void onDrawerClosed(View drawerView) {

        }

        @Override
        public void onDrawerStateChanged(int newState) {

        }
    };

    public void presentSwipeScreen() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.layout, swipeFragment);
        fragmentTransaction.commit();
        SparkerSingleton.getInstance().listener = swipeFragment;
    }

    public void dismissLoginScreen() {
        presentSwipeScreen();
        SparkerSingleton.getInstance().loadUsers();
    }

    public void presentLoginScreen() {
        Fragment fr = new LoginFragment();
        //fr.setArguments(args);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.layout, fr, "LoginFragment");
        fragmentTransaction.commit();
    }

    public void presentTopicsSelectionScreen() {
        Fragment fr = TopicsFragment.getInstance();

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.layout, fr, "TopicsFragment");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void logout() {
        AccessToken.setCurrentAccessToken(null);
        SparkerSingleton.getInstance().loggedOut();
        QBAuth.deleteSession().performAsync(new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                Log.d("MainActivity", "Successfully deleted session");
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("MainActivity", "Error deleting session");
            }
        });
        if (QBChatService.getInstance().isLoggedIn()) {
            QBChatService.getInstance().logout(new QBEntityCallback<Void>() {
                @Override
                public void onSuccess(Void aVoid, Bundle bundle) {
                    Log.d("MainActivity", "Successfully logged out of chat");
                }

                @Override
                public void onError(QBResponseException e) {
                    Log.d("MainActivity", "Error logging out of chat");
                }
            });
        }
        LoginManager.getInstance().logOut();
        CircleImageView menuProfileImageView = (CircleImageView) findViewById(R.id.menu_profile_image_view);
        menuProfileImageView.setImageResource(R.drawable.placeholder);
        presentLoginScreen();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        } else {
            // permission has been granted, continue as usual
            Location myLocation =
                    LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // We can now safely use the API we requested access to
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    SparkerSingleton.getInstance().currentLocation =
                            LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                }
            } else {
                Log.d("SparkerDebug", "Denied Location Access/Cancelled Request");
                // Permission was denied or request was cancelled
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void updateAndroidSecurityProvider(Activity callingActivity) {
        try {
            ProviderInstaller.installIfNeeded(this);
        } catch (GooglePlayServicesRepairableException e) {
            // Thrown when Google Play Services is not installed, up-to-date, or enabled
            // Show dialog to allow users to install, update, or otherwise enable Google Play services.
            GooglePlayServicesUtil.getErrorDialog(e.getConnectionStatusCode(), callingActivity, 0);
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e("SecurityException", "Google Play Services not available.");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
    }

    // IBillingHandler implementation

    @Override
    public void onBillingInitialized() {
        /*
         * Called when BillingProcessor was initialized and it's ready to purchase
         */
        Log.d("SparkerDebug", "Billing initialized");
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        /*
         * Called when requested PRODUCT ID was successfully purchased
         */
        Log.d("SparkerDebug", "Product was purchased");
    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
        /*
         * Called when some error occurred. See Constants class for more details
         *
         * Note - this includes handling the case where the user canceled the buy dialog:
         * errorCode = Constants.BILLING_RESPONSE_RESULT_USER_CANCELED
         */
        Log.d("SparkerDebug", "Billing error!");
    }

    @Override
    public void onPurchaseHistoryRestored() {
        /*
         * Called when purchase history was restored and the list of all owned PRODUCT ID's
         * was loaded from Google Play
         */
        Log.d("SparkerDebug", "Purchase history was restored");
    }

    @Override
    public void onDestroy() {
        if (bp != null)
            bp.release();

        super.onDestroy();
    }

    private BroadcastReceiver pushBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            String from = intent.getStringExtra("from");
            Log.i("SparkerDebug", "Receiving message: " + message + ", from " + from);

            final NotificationManager mNotificationManager =
                    (NotificationManager) MyApplication.getAppContext().getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(MyApplication.getAppContext())
                            .setSmallIcon(R.drawable.icon_spark)
                            .setContentTitle(message)
                            .setContentText(message)
                            .setTicker(message);
            mNotificationManager.notify(10, mBuilder.build());
        }
    };

}
