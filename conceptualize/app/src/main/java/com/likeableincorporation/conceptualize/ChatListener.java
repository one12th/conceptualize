package com.likeableincorporation.conceptualize;

public interface ChatListener {
    void messageReceived(String dialogID);

    void chatDisconnected();

    void chatConnected();
}