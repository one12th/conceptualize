package com.likeableincorporation.conceptualize.view;

import android.content.Context;
import android.util.AttributeSet;

import com.joooonho.SelectableRoundedImageView;

/**
 * Created by SamHarrison on 17/02/2017.
 */

public class SquareRoundedImageView extends SelectableRoundedImageView {

    public SquareRoundedImageView(Context context) {
        super(context);
    }

    public SquareRoundedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareRoundedImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        setMeasuredDimension(width, width);
    }
}
