package com.likeableincorporation.conceptualize.async;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GeocoderAsyncTask extends AsyncTask<Double, Void, List<Address>> {

    private static final String TAG = GeocoderAsyncTask.class.getCanonicalName();
    private static final String UNKNOWN_LOCATION = "Unknown Location";

    private final Geocoder geocoder;
    private final Callback callback;

    public GeocoderAsyncTask(Context context, Callback callback) {
        this.geocoder = new Geocoder(context, Locale.getDefault());
        this.callback = callback;
    }

    @Override
    protected List<Address> doInBackground(Double... params) {
        double lat = params[0];
        double lon = params[1];

        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(lat, lon, 1);
        } catch (IOException e) {
            Log.e(TAG, "error getting addresses", e);
        }
        return addresses;
    }

    @Override
    protected void onPostExecute(List<Address> addresses) {
        super.onPostExecute(addresses);
        if (callback != null) {
            if (addresses == null || addresses.size() == 0) {
                callback.onLocationFound(UNKNOWN_LOCATION);
            } else if (addresses.get(0).getSubAdminArea() != null) {
                callback.onLocationFound(addresses.get(0).getSubAdminArea());
            } else if (addresses.get(0).getAdminArea() != null) {
                callback.onLocationFound(addresses.get(0).getAdminArea());
            } else {
                callback.onLocationFound(UNKNOWN_LOCATION);
            }
        }
    }

    public interface Callback {

        void onLocationFound(String message);

    }
}