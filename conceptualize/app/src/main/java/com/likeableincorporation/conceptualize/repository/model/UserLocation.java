package com.likeableincorporation.conceptualize.repository.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by SamHarrison on 03/01/2017.
 */
public class UserLocation {

    public String name = "";
    public Double latitude = 0.0;
    public Double longitude = 0.0;

    public UserLocation(String name, LatLng location) {
        this.name = name;
        this.latitude = location.latitude;
        this.longitude = location.longitude;
    }
}
