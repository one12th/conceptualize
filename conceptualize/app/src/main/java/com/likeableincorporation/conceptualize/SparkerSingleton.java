package com.likeableincorporation.conceptualize;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;

import com.likeableincorporation.conceptualize.fragment.SwipeFragment;
import com.likeableincorporation.conceptualize.repository.TinyDB;
import com.likeableincorporation.conceptualize.repository.model.UserLocation;
import com.likeableincorporation.conceptualize.repository.model.UserProfile;
import com.quickblox.auth.session.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBPrivacyListsManager;
import com.quickblox.chat.QBRoster;
import com.quickblox.chat.listeners.QBRosterListener;
import com.quickblox.chat.listeners.QBSubscriptionListener;
import com.quickblox.chat.model.QBPresence;
import com.quickblox.chat.model.QBPrivacyList;
import com.quickblox.chat.model.QBPrivacyListItem;
import com.quickblox.chat.model.QBRosterEntry;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;
import com.quickblox.messages.QBPushNotifications;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBEvent;
import com.quickblox.messages.model.QBNotificationType;

import org.jivesoftware.smack.roster.packet.RosterPacket;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by SamHarrison on 11/04/16.
 */
public class SparkerSingleton {

    private static final String TAG = "SparkerDebug";

    private static final String SHARED_PREFERENCES = "SparkerSettings";

    private static final String PRIVACY_VALUE = "public";

    private static final String FIELD_USER_ID = "user_id";
    private static final String CLASS_NAME_PROFILE = "profile";
    private static final String FIELD_MIN_AGE = "minAge";
    private static final String FIELD_MAX_AGE = "maxAge";
    private static final String FIELD_MAX_DISTANCE = "maxDistance";
    private static final String FIELD_ACTIVE_LOCATION = "activeLocation";
    private static final String FIELD_LOCATIONS = "locations";

    private static final String API_QUICKBLOX_URL = "https://api.quickblox.com/blobs/";

    private static SparkerSingleton instance;

    private UserProfile previousProfile;
    private QBRoster chatRoster;
    private Handler findMoreUsersHandler = new Handler();

    public ArrayList<UserProfile> cardProfiles = new ArrayList<>();
    public ArrayList<Integer> pendingUsers = new ArrayList<>();
    public ArrayList<Integer> loadedIDs = new ArrayList<>();
    public SwipeFragment listener;
    public UserProfile currentUserProfile;
    public QBSession session;
    public Location currentLocation;
    public QBPrivacyList blockList;

    public static SparkerSingleton getInstance() {
        if (instance == null) {
            instance = new SparkerSingleton();
        }
        return instance;
    }

    public void loadUsers() {
        if (currentUserProfile == null || currentUserProfile.location == null) {
            Log.d(TAG, "Can't load users, current location is null!");
            return;
        }
        invalidateLoadMoreUsersTimer();
        loadPendingUsers();
    }

    private void loadPendingUsers() {
        if (pendingUsers.size() == 0) {
            Log.d(TAG, "No pending profiles");
            loadNearbyUsers();
            return;
        }

        Log.d(TAG, "Loading pending user profiles");

        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
        requestBuilder.ne(FIELD_USER_ID, currentUserProfile.userID);
        requestBuilder.in(FIELD_USER_ID, TextUtils.join(",", pendingUsers));
        requestBuilder.nin(FIELD_USER_ID, TextUtils.join(",", currentUserProfile.swiped));
        requestBuilder.in(UserProfile.FIELD_USER_TOPIC, currentUserProfile.userTopic);
        requestBuilder.in(UserProfile.FIELD_INTERESTED_IN, String.format("2,%d", currentUserProfile.gender));
        if (currentUserProfile.interestedIn != 2) {
            requestBuilder.eq(UserProfile.FIELD_GENDER, currentUserProfile.interestedIn);
        }

        QBCustomObjects.getObjects(CLASS_NAME_PROFILE, requestBuilder).performAsync(new QBEntityCallback<ArrayList<QBCustomObject>>() {
            @Override
            public void onSuccess(ArrayList<QBCustomObject> customObjects, Bundle params) {
                Log.d(TAG, "Got pending profiles");

                for (QBCustomObject object : customObjects) {
                    if (!loadedIDs.contains(object.getUserId())) {
                        cardProfiles.add(new UserProfile(object));
                        loadedIDs.add(object.getUserId());
                    }
                }
                if (cardProfiles.size() < 100) {
                    loadNearbyUsers();
                }
                try {
                    listener.adapter.notifyDataSetChanged();
                } catch (Exception e) {
                    Log.d(TAG, "error notifying list", e);
                }
            }

            @Override
            public void onError(QBResponseException errors) {
                Log.d(TAG, "Error loading pending profiles");
            }
        });
    }

    private void loadNearbyUsers() {

        Log.d(TAG, "Loading nearby users");

        SharedPreferences settings = MyApplication.getAppContext().getSharedPreferences(SHARED_PREFERENCES, 0);

        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();

        requestBuilder.ne(FIELD_USER_ID, currentUserProfile.userID);
        requestBuilder.nin(FIELD_USER_ID, TextUtils.join(",", currentUserProfile.swiped));

        int maxDistance = settings.getInt(FIELD_MAX_DISTANCE, 50);
        int activeLocation = settings.getInt(FIELD_ACTIVE_LOCATION, 0);
        TinyDB tinydb = new TinyDB(MyApplication.getAppContext());
        ArrayList<UserLocation> locations = tinydb.getListObject(FIELD_LOCATIONS, UserLocation.class);
        if (activeLocation == 0 || activeLocation > locations.size()) {
            requestBuilder.near(UserProfile.FIELD_LOCATION, new Double[]{currentUserProfile.location.getLongitude(), currentUserProfile.location.getLatitude()}, maxDistance * 1000);
        } else {
            requestBuilder.near(UserProfile.FIELD_LOCATION, new Double[]{locations.get(activeLocation - 1).longitude, locations.get(activeLocation - 1).latitude}, maxDistance * 1000);
        }

        if (currentUserProfile.userTopic != null) {
            requestBuilder.in(UserProfile.FIELD_USER_TOPIC, currentUserProfile.userTopic);
        }

        requestBuilder.in(UserProfile.FIELD_INTERESTED_IN, String.format("2,%d", currentUserProfile.gender));
        if (currentUserProfile.interestedIn != 2) {
            requestBuilder.eq(UserProfile.FIELD_GENDER, currentUserProfile.interestedIn);
        }

        requestBuilder.gte(UserProfile.FIELD_AGE, settings.getInt(FIELD_MIN_AGE, 18));
        int maxAge = settings.getInt(FIELD_MAX_AGE, 60);
        if (maxAge < 60) {
            requestBuilder.lte(UserProfile.FIELD_AGE, maxAge);
        }

        QBCustomObjects.getObjects(CLASS_NAME_PROFILE, requestBuilder).performAsync(new QBEntityCallback<ArrayList<QBCustomObject>>() {
            @Override
            public void onSuccess(ArrayList<QBCustomObject> customObjects, Bundle params) {
                Log.d(TAG, "Successfully loaded nearby users");

                for (QBCustomObject object : customObjects) {
                    if (!pendingUsers.contains(object.getUserId()) && !loadedIDs.contains(object.getUserId())) {
                        cardProfiles.add(new UserProfile(object));
                        loadedIDs.add(object.getUserId());
                    }
                }

                if (cardProfiles.size() < 3) {
                    setupLoadMoreUsersTimer();
                }

                try {
                    listener.adapter.notifyDataSetChanged();
                } catch (Exception e) {
                    Log.d(TAG, "Error reloading list");
                }
            }

            @Override
            public void onError(QBResponseException errors) {
                Log.d(TAG, "Error loading nearby users");
            }
        });
    }

    public void setupLoadMoreUsersTimer() {
        Log.d(TAG, "Setup load more users timer");
        findMoreUsersHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadUsers();
            }
        }, 15000);
    }

    public void invalidateLoadMoreUsersTimer() {
        Log.d(TAG, "Load more users timer invalidated");
        findMoreUsersHandler.removeCallbacksAndMessages(null);
    }

    public List<UserProfile> getCardProfiles() {
        return Collections.synchronizedList(cardProfiles);
    }

    public void removeProfile(Boolean addToPrevious) {
        if (addToPrevious) {
            previousProfile = getCardProfiles().get(0);
        }
        SparkerSingleton.getInstance().currentUserProfile.increaseLiked();
        SparkerSingleton.getInstance().currentUserProfile.addToSwiped(getCardProfiles().get(0).userID + "");
        SparkerSingleton.getInstance().uploadCurrentUserProfile();
        getCardProfiles().remove(0);
        if (getCardProfiles().size() <= 3) {
            loadUsers();
        }
        listener.adapter.notifyDataSetChanged();
    }

    public void restorePreviousProfile() {
        getCardProfiles().add(0, previousProfile);
    }

    public String getPrivateURLForID(String id) {
        return API_QUICKBLOX_URL + id + "/download?token=" + getTokenFromURLForUID(id);
    }

    private String getTokenFromURLForUID(String id) {
        return QBFile.getPrivateUrlForUID(id).replace(API_QUICKBLOX_URL + id + "?token=", "");
    }

    public void uploadCurrentUserProfile() {
        QBCustomObjects.updateObject(SparkerSingleton.getInstance().currentUserProfile.object).performAsync(new QBEntityCallback<QBCustomObject>() {
            @Override
            public void onSuccess(QBCustomObject qbCustomObject, Bundle bundle) {
            }

            @Override
            public void onError(QBResponseException e) {
            }
        });
    }

    public void uploadCurrentUserProfile(final OnProfileUploadComplete listener) {
        QBCustomObjects.updateObject(SparkerSingleton.getInstance().currentUserProfile.object)
                .performAsync(new QBEntityCallback<QBCustomObject>() {
                    @Override
                    public void onSuccess(QBCustomObject qbCustomObject, Bundle bundle) {
                        listener.onProfileUploadComplete(false);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        listener.onProfileUploadComplete(true);
                    }
                });
    }


    private QBRosterListener rosterListener = new QBRosterListener() {
        @Override
        public void entriesDeleted(Collection<Integer> userIds) {

        }

        @Override
        public void entriesAdded(Collection<Integer> userIds) {

        }

        @Override
        public void entriesUpdated(Collection<Integer> userIds) {
            for (int userID : userIds) {
                if (!SparkerSingleton.getInstance().currentUserProfile.matched.contains(userID + "") && chatRoster.getEntry(userID).getType() == RosterPacket.ItemType.both) {
                    SparkerSingleton.getInstance().currentUserProfile.addToMatched(userID + "");
                    SparkerSingleton.getInstance().uploadCurrentUserProfile();
                    listener.showMatchScreen(userID);
                }
            }
        }

        @Override
        public void presenceChanged(QBPresence presence) {

        }
    };

    QBSubscriptionListener subscriptionListener = new QBSubscriptionListener() {
        @Override
        public void subscriptionRequested(int userId) {
            if (chatRoster.contains(userId)) {
                confirmContactRequest(userId);
            } else {
                pendingUsers.add(userId);
            }
        }
    };


    public void rosterSetup() {
        chatRoster = QBChatService.getInstance().getRoster(QBRoster.SubscriptionMode.mutual, subscriptionListener);
        processRequests();
        chatRoster.addRosterListener(rosterListener);

        QBPrivacyListsManager privacyListsManager = QBChatService.getInstance().getPrivacyListsManager();
        try {
            blockList = privacyListsManager.getPrivacyList(PRIVACY_VALUE);
            privacyListsManager.setPrivacyListAsDefault(PRIVACY_VALUE);
        } catch (Exception e) {
            Log.e(TAG, "Roster Setup", e);
        }
    }

    public void blockUser(int userID) {
        if (blockList == null) {
            blockList = new QBPrivacyList();
            blockList.setName(PRIVACY_VALUE);
        }

        List<QBPrivacyListItem> items = blockList.getItems();
        if (items == null) {
            items = new ArrayList<>();
        }

        QBPrivacyListItem newItem = new QBPrivacyListItem();
        newItem.setMutualBlock(true);
        newItem.setType(QBPrivacyListItem.Type.USER_ID);
        newItem.setValueForType(String.valueOf(userID));
        items.add(newItem);
        blockList.setItems(items);
    }

    private void processRequests() {
        if (chatRoster == null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    rosterSetup();
                }
            }, 5000);
            return;
        }

        for (QBRosterEntry entry : chatRoster.getEntries()) {
            if (entry.getType() != RosterPacket.ItemType.none) {
                continue;
            }
            if (SparkerSingleton.getInstance().currentUserProfile.swiped.contains(entry.getUserId() + "")) {
                rejectContactRequest(entry.getUserId());
            } else if (!pendingUsers.contains(entry.getUserId())) {
                pendingUsers.add(entry.getUserId());
            }
        }
    }

    public void rejectContactRequest(int userID) {
        try {
            chatRoster.reject(userID);
            if (pendingUsers.contains(userID)) {
                pendingUsers.remove(Integer.valueOf(userID));
            }
        } catch (Exception e) {
            Log.d(TAG, "Error rejecting contact request", e);
        }
    }

    public void addToContactList(int userID) {
        if (pendingUsers.contains(userID)) {
            confirmContactRequest(userID);
        } else if (chatRoster.contains(userID)) {
            try {
                chatRoster.subscribe(userID);
            } catch (Exception e) {
                Log.d(TAG, "Error subscribing", e);
            }

        } else {
            try {
                chatRoster.createEntry(userID, null);
                //chatRoster.subscribe(userID);
            } catch (Exception e) {
                Log.d(TAG, "Error createEntry", e);
            }
        }
    }

    public void confirmContactRequest(int userID) {
        try {
            chatRoster.confirmSubscription(userID);
            if (pendingUsers.contains(userID)) {
                sendMatchNotification(userID);
                pendingUsers.remove(Integer.valueOf(userID));
            }
            listener.showMatchScreen(userID);

        } catch (Exception e) {
            Log.d(TAG, "Error confirming contact request", e);
        }
    }

    private void sendMatchNotification(int userID) {
        StringifyArrayList<Integer> userIds = new StringifyArrayList<>();
        userIds.add(userID);
        QBEvent event = new QBEvent();
        event.setUserIds(userIds);
        event.setEnvironment(QBEnvironment.DEVELOPMENT);
        event.setNotificationType(QBNotificationType.PUSH);
        event.setMessage("You have a new match!");

        QBPushNotifications.createEvent(event).performAsync(new QBEntityCallback<QBEvent>() {
            @Override
            public void onSuccess(QBEvent qbEvent, Bundle bundle) {

            }

            @Override
            public void onError(QBResponseException e) {

            }
        });
    }

    public void resetCards() {
        cardProfiles.clear();
        loadedIDs.clear();
        if (listener != null && listener.adapter != null) {
            listener.adapter.notifyDataSetChanged();
        }
    }

    public void loggedOut() {
        resetCards();
        currentUserProfile = null;
        chatRoster = null;
    }

    public AlertDialog alertWithError(Activity activity, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        alertDialog.setTitle("Oops");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        return alertDialog;
    }

    public void playNotification() {
        Vibrator v = (Vibrator) MyApplication.getAppContext().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(500);
    }

}
