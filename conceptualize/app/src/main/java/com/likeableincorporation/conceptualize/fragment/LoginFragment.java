package com.likeableincorporation.conceptualize.fragment;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.likeableincorporation.conceptualize.R;
import com.likeableincorporation.conceptualize.SparkerSingleton;
import com.likeableincorporation.conceptualize.activity.MainActivity;
import com.likeableincorporation.conceptualize.repository.model.UserProfile;
import com.likeableincorporation.conceptualize.view.SquareImageViewH;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBProvider;
import com.quickblox.auth.session.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;

public class LoginFragment extends Fragment {

    CallbackManager callbackManager;
    KProgressHUD loginProgressHUD;
    LoginButton loginButton;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        SquareImageViewH largeLogoImageView = (SquareImageViewH) view.findViewById(R.id.large_logo);
        largeLogoImageView.getDrawable().setColorFilter(getResources().getColor(R.color.mainColor), PorterDuff.Mode.MULTIPLY);

        ImageView textLogoImageView = (ImageView) view.findViewById(R.id.large_text_logo);
        textLogoImageView.getDrawable().setColorFilter(getResources().getColor(R.color.mainColor), PorterDuff.Mode.SRC_ATOP);

        loginButton = (LoginButton) view.findViewById(R.id.login_button);
        loginButton.setReadPermissions("public_profile", "user_friends", "user_birthday");
        loginButton.setFragment(this);
        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loginProgressHUD = KProgressHUD.create(getActivity())
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .show();
                loginButton.setAlpha(0);
                handleFacebookSession();
            }

            @Override
            public void onCancel() {
                cancelLogin();
                //On Cancelled Login
            }

            @Override
            public void onError(FacebookException exception) {
                handleFacebookError(exception);
                cancelLogin();
            }
        });

        TextView termsText = (TextView) view.findViewById(R.id.terms_text);
        String termsString = (String) termsText.getText();
        int termsStart = termsString.indexOf("Terms of Use");
        int termsEnd = termsStart + "Terms of Use".length();

        int privacyStart = termsString.indexOf("Privacy Policy");
        int privacyEnd = privacyStart + "Privacy Policy".length();

        SpannableString ss = new SpannableString(termsText.getText());
        ClickableSpan termsClickable = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                TermsFragment fragment = new TermsFragment();
                Bundle args = new Bundle();
                args.putBoolean("isTerms", true);
                fragment.setArguments(args);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.layout, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setFakeBoldText(true);
                ds.setColor(getResources().getColor(R.color.blackColor));
                ds.setUnderlineText(false);
            }
        };

        ClickableSpan privacyClickable = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                TermsFragment fragment = new TermsFragment();
                Bundle args = new Bundle();
                args.putBoolean("isTerms", false);
                fragment.setArguments(args);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.layout, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setFakeBoldText(true);
                ds.setColor(getResources().getColor(R.color.blackColor));
                ds.setUnderlineText(false);
            }
        };

        ss.setSpan(termsClickable, termsStart, termsEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(privacyClickable, privacyStart, privacyEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        termsText.setText(ss);
        termsText.setMovementMethod(LinkMovementMethod.getInstance());

        return view;
    }

    private void handleFacebookSession() {
        if (AccessToken.getCurrentAccessToken() == null) {
            Log.d("LoginFragment", "Access token is null, cancelling login!");
            cancelLogin();
            return;
        }
        QBAuth.createSessionUsingSocialProvider(QBProvider.FACEBOOK, AccessToken.getCurrentAccessToken().getToken(), null).performAsync(new QBEntityCallback<QBSession>() {
            @Override
            public void onSuccess(QBSession qbSession, Bundle bundle) {
                Log.d("LoginFragment", "Successfully logged into QuickBlox");
                SparkerSingleton.getInstance().session = qbSession;
                final QBUser user = new QBUser();
                user.setId(qbSession.getUserId());
                user.setPassword(qbSession.getToken());
                QBChatService.getInstance().login(user, new QBEntityCallback() {
                    @Override
                    public void onSuccess(Object o, Bundle bundle) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loadUserProfile(user);
                            }
                        });
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        cancelLogin();
                        return;
                    }
                });
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("SparkerDebug", "Error logging into QuickBlox");
                cancelLogin();
            }
        });
    }

    private void loadUserProfile(QBUser user) {
        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
        requestBuilder.eq("user_id", user.getId());
        QBCustomObjects.getObjects("profile", requestBuilder).performAsync(new QBEntityCallback<ArrayList<QBCustomObject>>() {
            @Override
            public void onSuccess(ArrayList<QBCustomObject> qbCustomObjects, Bundle bundle) {
                if (qbCustomObjects.size() > 0) {
                    SparkerSingleton.getInstance().currentUserProfile = new UserProfile(qbCustomObjects.get(0));
                    //SparkerSingleton.getInstance().loadUsers();
                    hideProgressHUD();
                    ((MainActivity) getActivity()).presentSwipeScreen();
                } else {
                    final QBCustomObject customObject = new QBCustomObject();
                    customObject.setClassName("profile");
                    QBCustomObjects.createObject(customObject).performAsync(new QBEntityCallback<QBCustomObject>() {
                        @Override
                        public void onSuccess(QBCustomObject qbCustomObject, Bundle bundle) {
                            if (qbCustomObject == null) {
                                cancelLogin();
                                return;
                            }
                            qbCustomObject.setClassName("profile");
                            SparkerSingleton.getInstance().currentUserProfile = new UserProfile(qbCustomObject);
                            //SparkerSingleton.getInstance().loadUsers();
                            hideProgressHUD();
                            ((MainActivity) getActivity()).presentSwipeScreen();
                        }

                        @Override
                        public void onError(QBResponseException e) {
                            cancelLogin();
                        }
                    });
                }
            }

            @Override
            public void onError(QBResponseException e) {
                cancelLogin();
                return;
            }
        });
    }

    private void cancelLogin() {
        Log.d("SparkerDebug", "Login Cancelled");
        hideProgressHUD();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loginButton.setAlpha(1);
            }
        });
    }

    private void handleFacebookError(FacebookException exception) {
        Log.d("SparkerDebug", "Handling Facebook Error");
    }

    private void hideProgressHUD() {
        if (loginProgressHUD != null) {
            loginProgressHUD.dismiss();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
