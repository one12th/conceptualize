package com.likeableincorporation.conceptualize.view;

import android.content.Context;
import android.util.AttributeSet;

import com.daprlabs.cardstack.SwipeDeck;

/**
 * Created by SamHarrison on 15/07/16.
 */
public class SquareSwipeDeck extends SwipeDeck {

    public SquareSwipeDeck(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = width + 66;
        if (height > MeasureSpec.getSize(heightMeasureSpec)) {
            height = MeasureSpec.getSize(heightMeasureSpec);
            width = MeasureSpec.getSize(heightMeasureSpec) - 66;
        }

        setMeasuredDimension(width, height);
    }
}
