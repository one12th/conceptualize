package com.likeableincorporation.conceptualize.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.likeableincorporation.conceptualize.AppConfig;
import com.likeableincorporation.conceptualize.MyApplication;
import com.likeableincorporation.conceptualize.OnProfileUploadComplete;
import com.likeableincorporation.conceptualize.R;
import com.likeableincorporation.conceptualize.SparkerSingleton;
import com.likeableincorporation.conceptualize.activity.MainActivity;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;

import info.hoang8f.android.segmented.SegmentedGroup;
import io.apptik.widget.MultiSlider;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

    private SegmentedGroup genderControl;
    private SwitchCompat showMenSwitch;
    private SwitchCompat showWomenSwitch;
    private MultiSlider distanceSlider;
    private TextView distanceLabel;
    private int distanceValue = 50;
    private MultiSlider ageRangeSlider;
    private TextView ageRangeLabel;
    private int ageMin = 18;
    private int ageMax = 60;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        showMenSwitch = view.findViewById(R.id.show_men_switch);
        showWomenSwitch = view.findViewById(R.id.show_women_switch);

        int interestedIn = SparkerSingleton.getInstance().currentUserProfile.interestedIn;
        if (interestedIn == 2) {
            showMenSwitch.setChecked(true);
            showWomenSwitch.setChecked(true);
        } else if (interestedIn == 1) {
            showWomenSwitch.setChecked(true);
        } else if (interestedIn == 0) {
            showMenSwitch.setChecked(true);
        }


        genderControl = view.findViewById(R.id.gender_control);
        genderControl.check(genderControl.getChildAt(SparkerSingleton.getInstance().currentUserProfile.gender).getId());
        genderControl.setOnCheckedChangeListener(genderChanged);

        showMenSwitch.setOnCheckedChangeListener(interestedInListener);
        showWomenSwitch.setOnCheckedChangeListener(interestedInListener);

        SharedPreferences settings = MyApplication.getAppContext().getSharedPreferences("SparkerSettings", 0);

        distanceSlider = view.findViewById(R.id.distance_slider);
        distanceLabel = view.findViewById(R.id.distance_label);
        distanceSlider.setOnThumbValueChangeListener(distanceChanged);
        distanceSlider.getThumb(0).setValue(settings.getInt("maxDistance", 50));

        ageRangeSlider = view.findViewById(R.id.age_range_slider);
        ageRangeLabel = view.findViewById(R.id.age_range_label);
        ageRangeSlider.setOnThumbValueChangeListener(ageRangeChanged);

        ageRangeSlider.getThumb(0).setValue(settings.getInt("minAge", 18));
        ageRangeSlider.getThumb(1).setValue(settings.getInt("maxAge", 60));

        Button topicsSelectButton = view.findViewById(R.id.topics_button);
        topicsSelectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity activity = (MainActivity) getActivity();

                if (activity != null) {
                    activity.presentTopicsSelectionScreen();
                }
            }
        });

        Button logoutButton = view.findViewById(R.id.logout_button);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = (MainActivity) getActivity();

                if (activity != null) {
                    activity.logout();
                }
            }
        });

        Button creditsButton = view.findViewById(R.id.credits_button);
        if (!AppConfig.enableCredits) {
            creditsButton.setVisibility(View.GONE);
        }
        creditsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setTitle("Credits")
                        .setMessage("All icons made by FreePik (www.freepik.com) from FlatIcon (www.flaticon.com) and licensed under CC BY 3.0.")
                        .setPositiveButton("Dismiss", null)
                        .setNeutralButton("Visit FreePik", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Uri uri = Uri.parse("http://www.freepik.com");
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("Visit FlatIcon", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Uri uri = Uri.parse("http://www.flaticon.com");
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        })
                        .show();
            }
        });

        Button deleteButton = view.findViewById(R.id.delete_account_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDelete();
            }
        });

        ImageButton menuButton = view.findViewById(R.id.menu_button);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleMenu();
            }
        });

        return view;
    }

    private CompoundButton.OnCheckedChangeListener interestedInListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (showMenSwitch.isChecked() && showWomenSwitch.isChecked()) {
                SparkerSingleton.getInstance().currentUserProfile.setInterestedIn(2);
            } else if (showMenSwitch.isChecked()) {
                SparkerSingleton.getInstance().currentUserProfile.setInterestedIn(0);
            } else if (showWomenSwitch.isChecked()) {
                SparkerSingleton.getInstance().currentUserProfile.setInterestedIn(1);
            } else {
                SparkerSingleton.getInstance().currentUserProfile.setInterestedIn(-1);
            }
            updateUsersSettings(buttonView.getContext());
        }
    };

    private RadioGroup.OnCheckedChangeListener genderChanged = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            SparkerSingleton.getInstance().currentUserProfile.setGender(group.indexOfChild(getActivity().findViewById(checkedId)));
            updateUsersSettings(group.getContext());
        }
    };

    private void updateUsersSettings(final Context context) {
        final KProgressHUD hud = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .show();
        SparkerSingleton.getInstance().uploadCurrentUserProfile(new OnProfileUploadComplete() {
            @Override
            public void onProfileUploadComplete(Boolean error) {
                hud.dismiss();
                if (error) {
                    new AlertDialog.Builder(context)
                            .setTitle(getString(R.string.alert_errortitle))
                            .setMessage(getString(R.string.settingsalert_errorupdatingsettings))
                            .setNeutralButton(getString(R.string.all_dismiss), null)
                            .show();
                }
            }
        });
    }

    private MultiSlider.OnThumbValueChangeListener distanceChanged = new MultiSlider.OnThumbValueChangeListener() {
        @Override
        public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
            distanceValue = value;
            distanceLabel.setText(distanceValue + "km");
        }
    };

    private MultiSlider.OnThumbValueChangeListener ageRangeChanged = new MultiSlider.OnThumbValueChangeListener() {
        @Override
        public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
            if (thumbIndex == 0) {
                ageMin = value;
            } else {
                ageMax = value;
            }
            String ageLabel = ageMin + " - " + ageMax;
            if (ageMax == 60) {
                ageLabel += "+";
            }
            ageRangeLabel.setText(ageLabel);
        }
    };

    private void confirmDelete() {
        new AlertDialog.Builder(getContext())
                .setTitle("Warning")
                .setMessage("Are you sure you want to delete your account?")
                .setNeutralButton("Dismiss", null)
                .setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteAccount();
                    }
                })
                .show();
    }

    private void deleteAccount() {
        QBUsers.deleteUser(QBChatService.getInstance().getUser().getId()).performAsync(new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                ((MainActivity) getActivity()).logout();
            }

            @Override
            public void onError(QBResponseException e) {
                new AlertDialog.Builder(getContext())
                        .setTitle(getString(R.string.alert_errortitle))
                        .setMessage(getString(R.string.settingsalert_errordeletingaccount))
                        .setNeutralButton(getString(R.string.all_dismiss), null)
                        .show();
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        SharedPreferences settings = getContext().getSharedPreferences("SparkerSettings", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("maxAge", ageMax);
        editor.putInt("minAge", ageMin);
        editor.putInt("maxDistance", distanceValue);
        editor.apply();
        ((MainActivity) getActivity()).swipeFragment.settingsChanged();
    }
}
