package com.likeableincorporation.conceptualize.fragment;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.likeableincorporation.conceptualize.R;
import com.likeableincorporation.conceptualize.activity.MainActivity;

import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class UpgradeDialogFragment extends SupportBlurDialogFragment {

    public UpgradeDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upgrade_dialog, container, false);

        Button upgradeButton = (Button) view.findViewById(R.id.upgrade_button);

        if (((MainActivity) getActivity()).bp != null && GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getContext()) == ConnectionResult.SUCCESS && ((MainActivity) getActivity()).bp.isSubscriptionUpdateSupported()) {
            upgradeButton.setText(R.string.upgradedialog_buttontext);
            upgradeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    purchaseSubscription();
                }
            });
        } else {
            upgradeButton.setText(R.string.upgradedialog_storeunavailable);
        }

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        return view;
    }

    private void purchaseSubscription() {
        ((MainActivity) getActivity()).bp.purchase(getActivity(), getString(R.string.iap_id));
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    protected boolean isDimmingEnable() {
        return true;
    }

}
