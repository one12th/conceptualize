package com.likeableincorporation.conceptualize.repository.model;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.likeableincorporation.conceptualize.AppConfig;
import com.quickblox.customobjects.model.QBCustomObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by SamHarrison on 15/07/16.
 */
public class UserProfile implements Parcelable {

    private static final String NULL_VALUE = "null";
    private static final String EMPTY = "";

    private static final String PROVIDER = "QB";

    private static final String FIELD_NAME = "name";
    private static final String FIELD_BIO = "bio";
    private static final String FIELD_MATCHED = "matched";
    private static final String FIELD_SWIPED = "swiped";
    private static final String FIELD_LIKED = "liked";
    private static final String FIELD_LIKE_LIMIT_LIFTED = "likeLimitLifted";

    public static final String FIELD_AGE = "age";
    public static final String FIELD_USER_TOPIC = "userTopic";
    public static final String FIELD_INTERESTED_IN = "interested_in";
    public static final String FIELD_GENDER = "gender";
    public static final String FIELD_LOCATION = "location";

    private static final SimpleDateFormat DEFAULT_DATE_FORMAT
            = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault());

    public QBCustomObject object;
    public int userID;
    public String name;
    public String bio;
    public Location location;
    public int age;
    public ArrayList<String> matched;
    public ArrayList<String> swiped;
    public String userTopic;
    public int gender;
    public int interestedIn;
    public int liked;
    public Date likeLimitLifted;
    public Date updatedAt;

    public UserProfile(QBCustomObject object) {
        super();
        this.object = object;
        userID = object.getUserId();
        name = (!object.getString(FIELD_NAME).equals(NULL_VALUE) ? object.getString(FIELD_NAME) : EMPTY);
        bio = (!object.getString(FIELD_BIO).equals(NULL_VALUE) ? object.getString(FIELD_BIO) : EMPTY);
        location = new Location(PROVIDER);
        try {
            List<String> locationList = (List<String>) (Object) object.getArray(FIELD_LOCATION);
            location.setLatitude(Double.parseDouble(locationList.get(1)));
            location.setLongitude(Double.parseDouble(locationList.get(0)));
        } catch (Exception e) {
            location.setLatitude(51.5072);
            location.setLongitude(0.1275);
        }

        try {
            age = object.getInteger(FIELD_AGE);
        } catch (Exception e) {
            age = 18;
        }

        if (object.getArray(FIELD_MATCHED) != null) {
            matched = (ArrayList<String>) (Object) object.getArray(FIELD_MATCHED);
        } else {
            matched = new ArrayList<>();
        }

        if (object.getArray(FIELD_SWIPED) != null) {
            swiped = (ArrayList<String>) (Object) object.getArray(FIELD_SWIPED);
        } else {
            swiped = new ArrayList<>();
        }

        try {
            userTopic = object.getString(FIELD_USER_TOPIC);
        } catch (Exception e) {
            userTopic = EMPTY;
        }

        try {
            gender = object.getInteger(FIELD_GENDER);
        } catch (Exception e) {
            gender = 0;
        }

        try {
            interestedIn = object.getInteger(FIELD_INTERESTED_IN);
        } catch (Exception e) {
            interestedIn = 1;
        }

        try {
            liked = object.getInteger(FIELD_LIKED);
        } catch (Exception e) {
            liked = 0;
        }

        try {
            likeLimitLifted = object.getDate(FIELD_LIKE_LIMIT_LIFTED, DEFAULT_DATE_FORMAT);
        } catch (Exception e) {
            likeLimitLifted = new Date();
        }

        if (likeLimitLifted == null) {
            likeLimitLifted = new Date();
        }

        updatedAt = object.getUpdatedAt();
    }

    public void setAge(int age) {
        this.age = age;
        this.object.putInteger(FIELD_AGE, age);
    }

    public void setBio(String bio) {
        this.bio = bio;
        this.object.putString(FIELD_BIO, bio);
    }

    public void setGender(int gender) {
        this.gender = gender;
        this.object.putInteger(FIELD_GENDER, gender);
    }

    public void setInterestedIn(int interestedIn) {
        this.interestedIn = interestedIn;
        this.object.putInteger(FIELD_INTERESTED_IN, interestedIn);
    }

    public void increaseLiked() {
        this.liked += 1;
        if (this.liked >= AppConfig.likeLimit) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.SECOND, AppConfig.likeLimitTime);
            setLikeLimit(cal.getTime());
            this.liked = 0;
        }
        this.object.putInteger(FIELD_LIKED, this.liked);
    }

    public void setLikeLimit(Date date) {
        this.likeLimitLifted = date;
        this.object.putDate(FIELD_LIKE_LIMIT_LIFTED, date);
    }

    public void setLocation(Location location) {
        this.location = location;
        List<Double> locationArray = new ArrayList<>();
        locationArray.add(location.getLongitude());
        locationArray.add(location.getLatitude());
        this.object.putArray(FIELD_LOCATION, locationArray);
    }

    public void addToMatched(String userID) {
        this.matched.add(userID);
        this.object.putArray(FIELD_MATCHED, matched);
    }

    public void setName(String name) {
        this.name = name;
        this.object.putString(FIELD_NAME, name);
    }

    public void addToSwiped(String userID) {
        this.swiped.add(userID);
        this.object.putArray(FIELD_SWIPED, swiped);
    }

    public void setUserTopic(String userTopic) {
        this.userTopic = userTopic;
        this.object.putString(FIELD_USER_TOPIC, userTopic);
    }

    public void setUserID(int userID) {
        this.userID = userID;
        this.object.setUserId(userID);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(userID);
        dest.writeString(name);
        dest.writeString(bio);
        dest.writeList(Arrays.asList(location.getLongitude(), location.getLatitude()));
        dest.writeInt(age);
        dest.writeList(matched);
        dest.writeList(swiped);
        dest.writeString(userTopic);
        dest.writeInt(gender);
        dest.writeInt(interestedIn);
        dest.writeInt(liked);
        dest.writeLong(updatedAt.getTime());
    }

    private UserProfile(Parcel in) {
        userID = in.readInt();
        name = in.readString();
        bio = in.readString();
        List loc = new ArrayList();
        in.readList(loc, null);
        location = new Location(PROVIDER);
        location.setLongitude((Double) loc.get(0));
        location.setLatitude((Double) loc.get(1));
        age = in.readInt();
        matched = new ArrayList<>();
        in.readList(matched, null);
        swiped = new ArrayList<>();
        in.readList(swiped, null);
        userTopic = in.readString();
        gender = in.readInt();
        interestedIn = in.readInt();
        liked = in.readInt();
        updatedAt = new Date(in.readInt());
    }

    public static final Parcelable.Creator<UserProfile> CREATOR
            = new Parcelable.Creator<UserProfile>() {

        // This simply calls our new constructor (typically private) and
        // passes along the unmarshalled `Parcel`, and then returns the new object!
        @Override
        public UserProfile createFromParcel(Parcel in) {
            return new UserProfile(in);
        }

        // We just need to copy this and change the type to match our class.
        @Override
        public UserProfile[] newArray(int size) {
            return new UserProfile[size];
        }
    };
}
