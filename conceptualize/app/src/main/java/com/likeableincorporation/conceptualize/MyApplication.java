package com.likeableincorporation.conceptualize;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.multidex.MultiDex;

import com.quickblox.auth.session.QBSettings;
import com.quickblox.chat.QBChatService;

/**
 * Created by SamHarrison on 15/07/16.
 */
public class MyApplication extends Application {

    private static Context context;
    private static MyApplication instance;

    public static MyApplication get() {
        return instance;
    }

    public SharedPreferences settings;

    @Override
    public void onCreate() {
        super.onCreate();

        MyApplication.context = getApplicationContext();

        StringXORer XORer = new StringXORer();

        QBSettings.getInstance().init(getApplicationContext(), getString(R.string.qb_app_id),
                XORer.decode(getString(R.string.qb_1), getString(R.string.qb_obs)),
                XORer.decode(getString(R.string.qb_2), getString(R.string.qb_obs)));
        QBSettings.getInstance().setAccountKey(XORer.decode(getString(R.string.qb_3), getString(R.string.qb_obs)));

        QBChatService.setDebugEnabled(true);
        //QBChatService.setDefaultAutoSendPresenceInterval(20);
        QBChatService.getInstance().startAutoSendPresence(60);

        QBChatService.getInstance().setUseStreamManagement(true);

        QBChatService.ConfigurationBuilder chatServiceConfigurationBuilder = new QBChatService.ConfigurationBuilder();
        chatServiceConfigurationBuilder.setSocketTimeout(60); //Sets chat socket's read timeout in seconds
        chatServiceConfigurationBuilder.setKeepAlive(true); //Sets connection socket's keepAlive option.
        chatServiceConfigurationBuilder.setUseTls(false); //Sets the TLS security mode used when making the connection. By default TLS is disabled.
        QBChatService.setConfigurationBuilder(chatServiceConfigurationBuilder);

        QBChatService.getInstance().addConnectionListener(ChatService.getInstance());
    }

    public static Context getAppContext() {
        return MyApplication.context;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
