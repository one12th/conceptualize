package com.likeableincorporation.conceptualize.fragment;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.likeableincorporation.conceptualize.R;
import com.likeableincorporation.conceptualize.SparkerSingleton;
import com.likeableincorporation.conceptualize.repository.model.UserProfile;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class MatchedFragment extends SupportBlurDialogFragment {

    UserProfile opponent;

    public MatchedFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_matched, container, false);

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            opponent = bundle.getParcelable("profile");
        }

        TextView subtitle = view.findViewById(R.id.match_subtitle);
        CircleImageView userImageView = view.findViewById(R.id.current_user_imageview);
        userImageView.setImageResource(R.drawable.placeholder);

        CircleImageView opponentImageView = view.findViewById(R.id.opponent_imageview);
        opponentImageView.setImageResource(R.drawable.placeholder);

        if (opponent != null) {
            subtitle.setText(String.format(getString(R.string.matchscreen_subheading), opponent.name));
        } else {
            subtitle.setText(String.format(getString(R.string.matchscreen_subheading), getString(R.string.matchscreen_someone)));
        }

        return view;
    }

    @Override
    protected boolean isDimmingEnable() {
        return true;
    }

}
