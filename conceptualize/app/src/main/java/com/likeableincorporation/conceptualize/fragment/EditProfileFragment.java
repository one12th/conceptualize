package com.likeableincorporation.conceptualize.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.baoyz.actionsheet.ActionSheet;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.likeableincorporation.conceptualize.OnProfileUploadComplete;
import com.likeableincorporation.conceptualize.R;
import com.likeableincorporation.conceptualize.SparkerSingleton;
import com.likeableincorporation.conceptualize.view.SquareImageButton;
import com.likeableincorporation.conceptualize.view.SquareImageView;
import com.quickblox.content.QBContent;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileFragment extends Fragment {

    private ArrayList<SquareImageButton> imageButtons = new ArrayList<SquareImageButton>();
    private Uri outputFileURI;
    private int PICK_IMAGE_REQUEST = 1;
    ArrayList<SquareImageView> gridImageViews = new ArrayList<SquareImageView>();

    public EditProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);

        TextView bioHeading = view.findViewById(R.id.bio_heading_label);
        bioHeading.setText("About " + SparkerSingleton.getInstance().currentUserProfile.name);

        final EditText bioEditText = view.findViewById(R.id.bio_edit_text);
        bioEditText.setText(SparkerSingleton.getInstance().currentUserProfile.bio);

        Button cancelButton = view.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        Button saveButton = view.findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final KProgressHUD progressHUD = KProgressHUD.create(getActivity())
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .show();
                SparkerSingleton.getInstance().currentUserProfile.setBio(bioEditText.getText().toString());
                SparkerSingleton.getInstance().uploadCurrentUserProfile(new OnProfileUploadComplete() {
                    @Override
                    public void onProfileUploadComplete(Boolean error) {
                        progressHUD.dismiss();
                        if (error) {
                            SparkerSingleton.getInstance()
                                    .alertWithError(getActivity(), "There was an error saving your profile.").show();
                        } else {
                            getFragmentManager().popBackStack();
                        }
                    }
                });
            }
        });

        return view;
    }

    private void requestPermissions() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    0);
        } else if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    0);
        } else if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA},
                    0);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        requestPermissions();
    }

    private ActionSheet.ActionSheetListener newListener = new ActionSheet.ActionSheetListener() {
        @Override
        public void onDismiss(ActionSheet actionSheet, boolean isCancel) {
            //
        }

        @Override
        public void onOtherButtonClick(ActionSheet actionSheet, int index) {

            if (index == 0) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File outputDir = getContext().getExternalCacheDir(); // context being the Activity pointer
                try {
                    File outputFile = File.createTempFile("tempphoto", "jpg", outputDir);
                    outputFileURI = Uri.fromFile(outputFile);
                    takePicture.putExtra(MediaStore.EXTRA_OUTPUT, outputFileURI);
                    startActivityForResult(takePicture, PICK_IMAGE_REQUEST);
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
            } else {
                Intent intent = new Intent();
                // Show only images, no videos or anything else
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                // Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        }
    };

    private ActionSheet.ActionSheetListener existingListener = new ActionSheet.ActionSheetListener() {
        @Override
        public void onDismiss(ActionSheet actionSheet, boolean isCancel) {
            //
        }

        @Override
        public void onOtherButtonClick(ActionSheet actionSheet, int index) {
            int position = Integer.parseInt(actionSheet.getTag());
        }
    };

    private void deleteImage(final String imageID) {
        final KProgressHUD progressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .show();

        SparkerSingleton.getInstance().uploadCurrentUserProfile(new OnProfileUploadComplete() {
            @Override
            public void onProfileUploadComplete(Boolean error) {
                progressHUD.dismiss();
                if (!error) {
                    QBContent.deleteFile(Integer.parseInt(imageID)).performAsync(null);
                } else {
                    SparkerSingleton.getInstance()
                            .alertWithError(getActivity(), "There was an error deleting your picture, please try again.").show();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            if (outputFileURI != null) {
                CropImage.activity(outputFileURI)
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .setFixAspectRatio(true)
                        .setRequestedSize(500, 500, CropImageView.RequestSizeOptions.RESIZE_INSIDE)
                        .start(getContext(), this);
            } else if (data != null && data.getData() != null) {
                CropImage.activity(data.getData())
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .setFixAspectRatio(true)
                        .setRequestedSize(500, 500, CropImageView.RequestSizeOptions.RESIZE_INSIDE)
                        .start(getContext(), this);
            }

        } else {

            Log.d("SparkerDebug", "Something went wrong picking image!");

        }
    }

}
