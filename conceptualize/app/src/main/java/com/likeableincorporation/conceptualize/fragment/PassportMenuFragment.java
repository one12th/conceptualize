package com.likeableincorporation.conceptualize.fragment;


import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.likeableincorporation.conceptualize.MyApplication;
import com.likeableincorporation.conceptualize.R;
import com.likeableincorporation.conceptualize.activity.MainActivity;
import com.likeableincorporation.conceptualize.repository.TinyDB;
import com.likeableincorporation.conceptualize.repository.model.UserLocation;
import com.likeableincorporation.conceptualize.view.SquareImageButton;

import java.util.ArrayList;

import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class PassportMenuFragment extends SupportBlurDialogFragment {

    private LayoutInflater mInflater;
    private ArrayList<UserLocation> locations;
    private int buttonX;
    private int buttonY;
    private int buttonWidth;
    private int buttonHeight;
    private LinearLayout locationMenuView;
    private ListView locationsListView;

    public PassportMenuFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);

            dialog.getWindow().setWindowAnimations(R.style.DialogNoAnimation);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mInflater = inflater;
        View view = mInflater.inflate(R.layout.fragment_passport_menu, container, false);

        TinyDB tinydb = new TinyDB(getContext());
        locations = (ArrayList<UserLocation>) (Object) tinydb.getListObject("locations", UserLocation.class);

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        locationsListView = (ListView) view.findViewById(R.id.location_list_view);
        locationsListView.setAdapter(locationsAdapter);
        locationsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TinyDB tinydb = new TinyDB(getContext());
                tinydb.putListObject("locations", (ArrayList<Object>) (Object) locations);
                SharedPreferences settings = MyApplication.getAppContext().getSharedPreferences("SparkerSettings", 0);
                SharedPreferences.Editor editor = settings.edit();
                if (position < locations.size()) {
                    editor.putInt("activeLocation", position + 1);
                } else {
                    editor.putInt("activeLocation", 0);
                }
                editor.commit();
                PassportMenuFragment.this.dismiss();
                ((MainActivity) getActivity()).swipeFragment.settingsChanged();
                ((MainActivity) getActivity()).swipeFragment.adapter.notifyDataSetInvalidated();
                ((MainActivity) getActivity()).swipeFragment.adapter.notifyDataSetChanged();
                ((MainActivity) getActivity()).swipeFragment.onResume();
            }
        });

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            buttonX = bundle.getInt("x");
            buttonY = bundle.getInt("y");
            buttonWidth = bundle.getInt("width");
            buttonHeight = bundle.getInt("height");
        }

        SquareImageButton closeButton = (SquareImageButton) view.findViewById(R.id.close_button);
        closeButton.setX(buttonX);
        closeButton.setY(buttonY);
        closeButton.setLayoutParams(new FrameLayout.LayoutParams(buttonWidth, buttonHeight));

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PassportMenuFragment.this.dismiss();
            }
        });

        Button newLocationButton = (Button) view.findViewById(R.id.new_location_button);
        newLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*FragmentManager fm = getFragmentManager();
                PassportFragment passportFragment = new PassportFragment();
                passportFragment.show(fm,"fragment_passport");
                LikeLimitDialogFragment.this.dismiss();*/

                PassportFragment passportFragment = new PassportFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.layout, passportFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                PassportMenuFragment.this.dismiss();
            }
        });

        locationMenuView = (LinearLayout) view.findViewById(R.id.location_menu_view);

        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (locationMenuView.getMeasuredHeight() > buttonY - 100) {
                            locationMenuView.getLayoutParams().height = buttonY - 100;
                        }
                        locationMenuView.setY(buttonY - locationMenuView.getMeasuredHeight() - 25);
                    }
                });
            }
        });

        return view;
    }

    @Override
    protected boolean isDimmingEnable() {
        return true;
    }

    private BaseAdapter locationsAdapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return locations.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View rowView;
            SharedPreferences settings = getContext().getSharedPreferences("SparkerSettings", 0);
            int activeLocation = settings.getInt("activeLocation", 0);
            if (position < locations.size()) {
                rowView = mInflater.inflate(R.layout.location_menu_item, parent, false);
                TextView locationTextView = (TextView) rowView.findViewById(R.id.location_text_view);
                ImageView locationIcon = (ImageView) rowView.findViewById(R.id.location_image_view);
                ImageView itemTick = (ImageView) rowView.findViewById(R.id.location_tick);
                Button deleteButton = (Button) rowView.findViewById(R.id.delete_button);

                deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TinyDB tinydb = new TinyDB(getContext());
                        locations.remove(position);
                        tinydb.putListObject("locations", (ArrayList<Object>) (Object) locations);
                        locationsAdapter.notifyDataSetChanged();
                    }
                });
                locationIcon.setImageResource(R.drawable.icon_plane);
                locationTextView.setText(locations.get(position).name);
                if (activeLocation == position + 1) {
                    itemTick.setVisibility(View.VISIBLE);
                } else {
                    itemTick.setVisibility(View.INVISIBLE);
                }
            } else {
                rowView = mInflater.inflate(R.layout.current_location_menu_item, parent, false);
                TextView locationTextView = (TextView) rowView.findViewById(R.id.location_text_view);
                ImageView locationIcon = (ImageView) rowView.findViewById(R.id.location_image_view);
                ImageView itemTick = (ImageView) rowView.findViewById(R.id.location_tick);
                locationIcon.setImageResource(R.drawable.icon_location);
                locationTextView.setText("Current Location");
                if (activeLocation == 0) {
                    itemTick.setVisibility(View.VISIBLE);
                } else {
                    itemTick.setVisibility(View.INVISIBLE);
                }
            }

            return rowView;
        }
    };

}
