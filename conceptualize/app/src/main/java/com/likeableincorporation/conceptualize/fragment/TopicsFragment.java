package com.likeableincorporation.conceptualize.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.likeableincorporation.conceptualize.OnProfileUploadComplete;
import com.likeableincorporation.conceptualize.R;
import com.likeableincorporation.conceptualize.SparkerSingleton;
import com.likeableincorporation.conceptualize.adapter.TopicsAdapter;

/**
 * Adds or Modify Topics
 *
 * @author Darien
 */
public class TopicsFragment extends Fragment implements View.OnClickListener, TopicsAdapter.OnItemSelected {

    private static final String TAG = TopicsFragment.class.getCanonicalName();

    private RecyclerView recyclerView;
    private TopicsAdapter topicsAdapter;

    public static TopicsFragment getInstance() {
        return new TopicsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_topics, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.save_button).setOnClickListener(this);

        recyclerView = view.findViewById(R.id.recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // adding divider
        recyclerView.addItemDecoration(new DividerItemDecoration(view.getContext(), LinearLayoutManager.VERTICAL));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        fillListOfTopics();
    }

    private void fillListOfTopics() {
        String [] topics = getResources().getStringArray(R.array.topics);
        int position = 0;

        String topic = SparkerSingleton.getInstance().currentUserProfile.userTopic;
        if (!TextUtils.isEmpty(topic)) {
            position = findPosition(topics, topic);
        }

        topicsAdapter = new TopicsAdapter(topics, position, this);
        recyclerView.setAdapter(topicsAdapter);
    }

    private int findPosition(String[] topics, String topic) {
        for(int i = 0; i < topics.length; i++) {
            if (topics[i].equalsIgnoreCase(topic)) {
                return i;
            }
        }

        return 0;
    }

    @Override
    public void onItemSelected(int position, String value) {
        Log.d(TAG, "Selected position: " + position + " value: " + value);
    }

    @Override
    public void onClick(View view) {
        // on save btn click
        String topic = topicsAdapter.getSelectedValue();

        if (topic != null) {
            SparkerSingleton.getInstance().currentUserProfile.setUserTopic(topic);
            updateUsersSettings(view.getContext());
        }
    }

    private void updateUsersSettings(final Context context) {
        final KProgressHUD hud = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .show();

        SparkerSingleton.getInstance().uploadCurrentUserProfile(new OnProfileUploadComplete() {
            @Override
            public void onProfileUploadComplete(Boolean error) {
                hud.dismiss();

                if (error) {
                    new AlertDialog.Builder(context)
                            .setTitle(getString(R.string.alert_errortitle))
                            .setMessage(getString(R.string.settingsalert_errorupdatingsettings))
                            .setNeutralButton(getString(R.string.all_dismiss), null)
                            .show();
                } else {
                    // navigate to settings fragment
                    FragmentManager fragmentManager = getFragmentManager();
                    if (fragmentManager != null) {
                        fragmentManager.popBackStack();
                    }
                }
            }
        });
    }
}
