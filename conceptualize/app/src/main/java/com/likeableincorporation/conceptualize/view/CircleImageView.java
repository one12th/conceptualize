package com.likeableincorporation.conceptualize.view;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by SamHarrison on 07/11/2016.
 */
public class CircleImageView extends de.hdodenhof.circleimageview.CircleImageView {

    public CircleImageView(Context context) {
        super(context);
    }

    public CircleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircleImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        setMeasuredDimension(width, width);
    }

}
