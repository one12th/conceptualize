package com.likeableincorporation.conceptualize.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import com.likeableincorporation.conceptualize.R;

/**
 * View adapter to fill the list of topics
 * @see com.likeableincorporation.conceptualize.fragment.TopicsFragment
 *
 * @author Darien
 */
public class TopicsAdapter extends RecyclerView.Adapter<TopicsAdapter.TopicsViewHolder> {

    /**
     * Callback interface
     */
    public interface OnItemSelected {

        /**
         * On Item selected
         *
         * @param position position of the item in the list
         * @param value value of the item
         */
        void onItemSelected(int position, String value);
    }

    private final OnItemSelected callback;
    private final String[] topics;

    private int selectedPosition;

    public TopicsAdapter(String[] topics, int selectedPosition, OnItemSelected callback) {
        this.topics = topics;
        this.selectedPosition = selectedPosition;
        this.callback = callback;
    }

    @NonNull
    @Override
    public TopicsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TopicsViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.topic_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TopicsViewHolder holder, int position) {
        holder.checkedTextView.setText(topics[position]);
        holder.checkedTextView.setTag(position);

        boolean checked = position == selectedPosition;
        holder.checkedTextView.setChecked(checked);
    }

    @Override
    public int getItemCount() {
        return topics.length;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public String getSelectedValue() {
        if (selectedPosition >= 0 && selectedPosition < getItemCount()) {
            return topics[selectedPosition];
        }

        return null;
    }

    /**
     * View holder
     */
    class TopicsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CheckedTextView checkedTextView;

        TopicsViewHolder(View itemView) {
            super(itemView);

            checkedTextView = itemView.findViewById(R.id.checked_text_View);
            checkedTextView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = (Integer) view.getTag();

            if (selectedPosition != position) {
                selectedPosition = position;

                checkedTextView.setChecked(true);

                if (callback != null) {
                    callback.onItemSelected(selectedPosition, topics[selectedPosition]);
                }

                notifyDataSetChanged();
            }
        }
    }
}
