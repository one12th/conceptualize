package com.likeableincorporation.conceptualize;

/**
 * Created by SamHarrison on 20/02/2017.
 */

public class AppConfig {

    public static boolean shouldLimitLikes = true; //Set to false to allow free users unlimited likes
    public static int likeLimit = 100; //Number of likes a user can make before they must upgrade or wait
    public static int likeLimitTime = 24 * 60 * 60; //Time in seconds user has to wait for likes to be refreshed

    public static boolean rewindEnabled = true; //Set to false to disabled rewind completely
    public static boolean rewindIsPremium = true; //Set to false to allow free users use of rewind

    public static boolean passportEnabled = true; //Set to false to disable passport completely
    public static boolean passportIsPremium = true; //Set to false to allow free users use of passport

    public static boolean enableCredits = true; //Set to false to disable credits popup
}
