package com.likeableincorporation.conceptualize.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.likeableincorporation.conceptualize.ChatService;
import com.likeableincorporation.conceptualize.R;
import com.likeableincorporation.conceptualize.activity.MainActivity;
import com.likeableincorporation.conceptualize.repository.model.UserProfile;
import com.likeableincorporation.conceptualize.view.CircleImageView;
import com.quickblox.chat.model.QBChatDialog;

import static android.text.format.DateUtils.getRelativeTimeSpanString;


/**
 * A simple {@link Fragment} subclass.
 */
public class DialogsFragment extends Fragment {

    private LayoutInflater mInflater;
    private ListView listView;

    public DialogsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mInflater = inflater;

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dialogs, container, false);

        listView = (ListView) view.findViewById(R.id.dialogs_list_view);
        listView.setAdapter(mListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ChatFragment fragment = new ChatFragment();
                fragment.dialog = ChatService.getInstance().dialogs.get(position);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.layout, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        ImageButton backButton = (ImageButton) view.findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.layout, ((MainActivity) getActivity()).swipeFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver, new IntentFilter("dialogs-updated"));

        return view;
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mListAdapter.notifyDataSetChanged();
        }
    };

    private BaseAdapter mListAdapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return ChatService.getInstance().dialogs.size();
        }

        @Override
        public Object getItem(int position) {
            return ChatService.getInstance().dialogs.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View rowView = mInflater.inflate(R.layout.item_dialog, parent, false);
            TextView nameTextView = (TextView) rowView.findViewById(R.id.name);
            CircleImageView imageView = (CircleImageView) rowView.findViewById(R.id.image_view);
            TextView lastMessageTextView = (TextView) rowView.findViewById(R.id.last_message);
            TextView lastMessageDateTextView = (TextView) rowView.findViewById(R.id.last_message_date);
            Button deleteButton = (Button) rowView.findViewById(R.id.delete_chat_button);

            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("SparkerDebug", "Clicked the delete chat button!");
                    ChatService.getInstance().deleteDialog(ChatService.getInstance().dialogs.get(position));
                    mListAdapter.notifyDataSetChanged();
                }
            });

            QBChatDialog dialog = (QBChatDialog) getItem(position);
            UserProfile dialogOpponent = ChatService.getInstance().getDialogOpponent(dialog);
            imageView.setImageResource(R.drawable.placeholder);

            if (dialogOpponent != null) {
                nameTextView.setText(dialogOpponent.name);
            }

            if (dialog.getLastMessage() == null && dialog.getCreatedAt() != null) {
                lastMessageTextView.setText(String.format(getResources().getString(R.string.dialogsfragment_matcheddate), getRelativeTimeSpanString(getContext(), dialog.getCreatedAt().getTime(), true)));
            } else {
                lastMessageTextView.setText(dialog.getLastMessage());
            }

            if (dialog.getLastMessageDateSent() != 0) {
                lastMessageDateTextView.setText(getRelativeTimeSpanString(getContext(), dialog.getLastMessageDateSent() * 1000, false));
            } else {
                lastMessageDateTextView.setText("");
            }

            if (dialogOpponent != null && dialog.getUnreadMessageCount() != null && dialog.getUnreadMessageCount() > 0 && dialog.getLastMessageUserId() != null && dialog.getLastMessageUserId() == dialogOpponent.userID) {
                lastMessageTextView.setTypeface(null, Typeface.BOLD);
                rowView.setBackgroundColor(getResources().getColor(R.color.lightestGray));
            }

            return rowView;
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mMessageReceiver);
    }
}
