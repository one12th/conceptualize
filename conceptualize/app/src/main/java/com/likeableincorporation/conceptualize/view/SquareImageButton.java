package com.likeableincorporation.conceptualize.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;

import com.likeableincorporation.conceptualize.R;

/**
 * Created by SamHarrison on 22/07/16.
 */
public class SquareImageButton extends AppCompatImageButton {

    private boolean mUseWidth = true;

    public SquareImageButton(Context context) {
        super(context);
    }

    public SquareImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.SquareImageButton,
                0, 0);

        try {
            mUseWidth = a.getBoolean(R.styleable.SquareImageButton_useWidth, true);
        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int dim;
        if (mUseWidth) {
            dim = getMeasuredWidth();
        } else {
            dim = getMeasuredHeight();
        }
        setMeasuredDimension(dim, dim);
    }
}