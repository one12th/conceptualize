package com.likeableincorporation.conceptualize.fragment;


import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.likeableincorporation.conceptualize.ChatService;
import com.likeableincorporation.conceptualize.R;
import com.likeableincorporation.conceptualize.SparkerSingleton;
import com.likeableincorporation.conceptualize.activity.MainActivity;
import com.likeableincorporation.conceptualize.repository.model.UserProfile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;

import org.ocpsoft.prettytime.PrettyTime;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    UserProfile user;
    Boolean alreadyMatched;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);


        ImageButton backButton = view.findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        ImageButton likeButton = view.findViewById(R.id.like_button);
        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SwipeFragment swipeFragment = ((MainActivity) getActivity()).swipeFragment;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeFragment.cardStack.swipeTopCardRight(250);
                    }
                }, 250);
                getFragmentManager().popBackStackImmediate();
            }
        });

        ImageButton passButton = view.findViewById(R.id.pass_button);
        passButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SwipeFragment swipeFragment = ((MainActivity) getActivity()).swipeFragment;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeFragment.cardStack.swipeTopCardLeft(250);
                    }
                }, 250);
                getFragmentManager().popBackStackImmediate();
            }
        });

        Button editButton = view.findViewById(R.id.edit_button);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditProfileFragment fragment = new EditProfileFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.layout, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        Button reportButton = view.findViewById(R.id.report_button);
        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmReport();
            }
        });

        Bundle args = getArguments();
        alreadyMatched = !args.getBoolean("cards");
        Boolean ownProfile = args.getBoolean("own_profile");
        if (ownProfile) {
            this.user = SparkerSingleton.getInstance().currentUserProfile;
            editButton.setVisibility(View.VISIBLE);
        } else if (!alreadyMatched) {
            this.user = SparkerSingleton.getInstance().getCardProfiles().get(0);
            likeButton.setVisibility(View.VISIBLE);
            passButton.setVisibility(View.VISIBLE);
        } else {
            this.user = ChatService.getInstance().userProfiles.get(args.getInt("index"));
        }

        TextView nameAgeLabel = view.findViewById(R.id.name_age_label);
        nameAgeLabel.setText(String.format(getString(R.string.profile_nameage), this.user.name, this.user.age));

        TextView distanceLabel = view.findViewById(R.id.distance_label);
        distanceLabel.setText(String.format(getString(R.string.profile_distance), Math.round(this.user.location.distanceTo(SparkerSingleton.getInstance().currentUserProfile.location)) / 1000));

        TextView lastActiveLabel = view.findViewById(R.id.last_active_label);
        PrettyTime p = new PrettyTime();
        lastActiveLabel.setText(String.format(getString(R.string.profile_lastactive), p.format(this.user.updatedAt)));

        TextView bioHeadingLabel = view.findViewById(R.id.bio_heading_label);
        bioHeadingLabel.setText(String.format(getString(R.string.profile_aboutheading), this.user.name));

        TextView bioLabel = view.findViewById(R.id.bio_label);
        bioLabel.setText(this.user.bio);

        return view;
    }

    private void confirmReport() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getString(R.string.profile_reporttext))
                .setTitle(getString(R.string.profile_reportbutton));
        builder.setNeutralButton(getString(R.string.all_dismiss), null);
        builder.setNegativeButton(getString(R.string.profile_reportbutton), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                report();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void report() {
        QBCustomObject report = new QBCustomObject("reports");
        report.putString("reported_user", user.userID + "");
        QBCustomObjects.createObject(report).performAsync(new QBEntityCallback<QBCustomObject>() {
            @Override
            public void onSuccess(QBCustomObject qbCustomObject, Bundle bundle) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getString(R.string.profile_reportconfirmtext))
                        .setTitle(getString(R.string.profile_reportconfirmtitle));
                builder.setNeutralButton(getString(R.string.all_dismiss), null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public void onError(QBResponseException e) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getString(R.string.alert_generalerrortext))
                        .setTitle(getString(R.string.alert_errortitle));
                builder.setNeutralButton(getString(R.string.all_dismiss), null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

}


