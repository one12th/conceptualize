package com.likeableincorporation.conceptualize.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.daprlabs.cardstack.SwipeDeck;
import com.facebook.AccessToken;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.likeableincorporation.conceptualize.AppConfig;
import com.likeableincorporation.conceptualize.ChatService;
import com.likeableincorporation.conceptualize.MyApplication;
import com.likeableincorporation.conceptualize.OnProfileUploadComplete;
import com.likeableincorporation.conceptualize.R;
import com.likeableincorporation.conceptualize.SparkerSingleton;
import com.likeableincorporation.conceptualize.activity.MainActivity;
import com.likeableincorporation.conceptualize.repository.model.UserProfile;
import com.likeableincorporation.conceptualize.view.CircleImageView;
import com.likeableincorporation.conceptualize.view.SquareImageButton;
import com.likeableincorporation.conceptualize.view.SquareRoundedImageView;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBProvider;
import com.quickblox.auth.session.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;
import com.quickblox.users.model.QBUser;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 */
public class SwipeFragment extends Fragment implements LocationListener {

    public SwipeDeckAdapter adapter;
    public SwipeDeck cardStack;
    public KProgressHUD loginProgressHUD;
    public Boolean foundLocation;
    public ImageView profileImageView;
    public View messageIndicator;
    public SquareImageButton passportButton;

    private ImageButton menuButton;
    private ImageButton chatsButton;
    private ImageView animatedCircle;
    private SquareImageButton rewindButton;
    private SquareImageButton likeButton;
    private SquareImageButton passButton;

    public SwipeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loginProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Connecting")
                .show();

        if (SparkerSingleton.getInstance().currentUserProfile == null) {
            Log.d("SparkerDebug", "No currentUserProfile, so creating new session");
            createSession();
        } else {
            Log.d("SparkerDebug", "Have currentUserProfile so refreshing user");
            refreshCurrentUser();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_swipe, container, false);

        messageIndicator = view.findViewById(R.id.message_indicator);
        profileImageView = view.findViewById(R.id.profile_image_view);
        cardStack = view.findViewById(R.id.swipe_deck);
        passportButton = view.findViewById(R.id.passport_button);

        menuButton = view.findViewById(R.id.menu_button);
        chatsButton = view.findViewById(R.id.chats_button);
        animatedCircle = view.findViewById(R.id.animated_circle_view);
        rewindButton = view.findViewById(R.id.rewind_button);
        likeButton = view.findViewById(R.id.like_button);
        passButton = view.findViewById(R.id.pass_button);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleMenu();
            }
        });

        chatsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogsFragment fragment = new DialogsFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.layout, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        profileImageView.setImageResource(R.drawable.placeholder);

        CircleImageView menuProfileImageView = getActivity().findViewById(R.id.menu_profile_image_view);
        menuProfileImageView.setImageResource(R.drawable.placeholder);

        Animation expandAnim = AnimationUtils.loadAnimation(getContext(), R.anim.swipe_circle_anim);
        expandAnim.setRepeatCount(Animation.INFINITE);

        animatedCircle.startAnimation(expandAnim);

        adapter = new SwipeDeckAdapter(SparkerSingleton.getInstance().cardProfiles, getContext());
        cardStack.setAdapter(adapter);
        cardStack.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {
                Log.i("MainActivity", "card was swiped left, position in adapter: " + position);
                UserProfile profile = SparkerSingleton.getInstance().cardProfiles.get(0);
                SparkerSingleton.getInstance().rejectContactRequest(profile.userID);
                SparkerSingleton.getInstance().removeProfile(true);
            }

            @Override
            public void cardSwipedRight(int position) {

                Log.i("MainActivity", "card was swiped right, position in adapter: " + position);
                UserProfile profile = SparkerSingleton.getInstance().cardProfiles.get(0);
                SparkerSingleton.getInstance().addToContactList(profile.userID);
                SparkerSingleton.getInstance().removeProfile(false);
            }

            @Override
            public void cardsDepleted() {
                Log.i("MainActivity", "no more cards");
            }

            @Override
            public void cardActionDown() {
                Log.i("MainActivity", "card action down");
            }

            @Override
            public void cardActionUp() {
                Log.i("MainActivity", "card action up");
            }
        });

        SharedPreferences settings = MyApplication.getAppContext().getSharedPreferences("SparkerSettings", 0);

        if (settings.getInt("activeLocation", 0) == 0) {
            passportButton.setImageResource(R.drawable.icon_location);
        } else {
            passportButton.setImageResource(R.drawable.icon_plane);
        }

        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardStack.swipeTopCardRight(250);
            }
        });

        passButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardStack.swipeTopCardLeft(250);
            }
        });

        rewindButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SparkerSingleton.getInstance().restorePreviousProfile();
                cardStack.rewindToPreviousCard();
            }
        });

        if (!AppConfig.rewindEnabled) {
            rewindButton.setVisibility(View.INVISIBLE);
        }
        if (!AppConfig.passportEnabled) {
            passportButton.setVisibility(View.INVISIBLE);
        }

        passportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPassportMenu();
            }
        });

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mDialogsUpdatedNotification, new IntentFilter("dialogs-updated"));

        requestPermissions();

    }

    private void requestPermissions() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    0);
        }
    }

    public void showPassportMenu() {
        PassportMenuFragment passportMenuFragment = new PassportMenuFragment();

        Bundle bundle = new Bundle();

        View globalView = getView();

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int topOffset = dm.heightPixels - globalView.getMeasuredHeight();

        int[] coordinates = new int[2];
        passportButton.getLocationInWindow(coordinates);

        bundle.putInt("x", coordinates[0]);
        bundle.putInt("y", coordinates[1] - topOffset);
        bundle.putInt("width", passportButton.getMeasuredWidth());
        bundle.putInt("height", passportButton.getMeasuredHeight());

        passportMenuFragment.setArguments(bundle);

        FragmentManager fm = getFragmentManager();
        passportMenuFragment.show(fm, "fragment_passport");
    }

    @Override
    public void onResume() {
        super.onResume();
        checkForUnreadIndicator();
        if (SparkerSingleton.getInstance().currentUserProfile != null) {
            SparkerSingleton.getInstance().loadUsers();
        }
        SharedPreferences settings = MyApplication.getAppContext().getSharedPreferences("SparkerSettings", 0);
        if (settings.getInt("activeLocation", 0) == 0) {
            passportButton.setImageResource(R.drawable.icon_location);
        } else {
            passportButton.setImageResource(R.drawable.icon_plane);
        }
    }

    private void checkForUnreadIndicator() {
        if (ChatService.getInstance().shouldShowUnreadIndicator()) {
            messageIndicator.setVisibility(View.VISIBLE);
        } else {
            messageIndicator.setVisibility(View.INVISIBLE);
        }
    }

    private BroadcastReceiver mDialogsUpdatedNotification = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkForUnreadIndicator();
        }
    };

    public void createSession() {
        QBAuth.createSessionUsingSocialProvider(QBProvider.FACEBOOK, AccessToken.getCurrentAccessToken().getToken(), null).performAsync(new QBEntityCallback<QBSession>() {
            @Override
            public void onSuccess(QBSession qbSession, Bundle bundle) {
                Log.d("SparkerDebug", "Created session, now connecting to chat");
                SparkerSingleton.getInstance().session = qbSession;

                final QBUser user = new QBUser();
                user.setId(qbSession.getUserId());
                user.setPassword(qbSession.getToken());

                QBChatService.getInstance().login(user, new QBEntityCallback() {
                    @Override
                    public void onSuccess(Object o, Bundle bundle) {
                        Log.d("SparkerDebug", "Connected to chat, now loading user profile");
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loadUserProfile(user);
                            }
                        });
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        Log.d("SparkerDebug", "Error connecting to chat");
                        hideProgressHUD();
                        ((MainActivity) getActivity()).logout();
                    }
                });
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("SparkerDebug", "Error creating session");
                hideProgressHUD();
                ((MainActivity) getActivity()).logout();
            }
        });
    }

    private void loadUserProfile(QBUser user) {
        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
        requestBuilder.eq("user_id", user.getId());
        QBCustomObjects.getObjects("profile", requestBuilder).performAsync(new QBEntityCallback<ArrayList<QBCustomObject>>() {
            @Override
            public void onSuccess(ArrayList<QBCustomObject> qbCustomObjects, Bundle bundle) {
                if (qbCustomObjects.size() > 0) {
                    Log.d("SparkerDebug", "Loaded current user profile, will refresh from FB");
                    SparkerSingleton.getInstance().currentUserProfile = new UserProfile(qbCustomObjects.get(0));
                    //SparkerSingleton.getInstance().loadUsers();
                    refreshCurrentUser();

                } else {
                    Log.d("SparkerDebug", "Current user profile doesn't exist, creating new one.");
                    final QBCustomObject customObject = new QBCustomObject();
                    customObject.setClassName("profile");
                    QBCustomObjects.createObject(customObject).performAsync(new QBEntityCallback<QBCustomObject>() {
                        @Override
                        public void onSuccess(QBCustomObject qbCustomObject, Bundle bundle) {
                            if (qbCustomObject == null) {
                                Log.d("SparkerDebug", "Object wasn't created, null");
                                hideProgressHUD();
                                ((MainActivity) getActivity()).logout();
                                return;
                            }
                            Log.d("SparkerDebug", "Successfully created profile, refreshing user");
                            qbCustomObject.setClassName("profile");
                            SparkerSingleton.getInstance().currentUserProfile = new UserProfile(qbCustomObject);
                            refreshCurrentUser();
                        }

                        @Override
                        public void onError(QBResponseException e) {
                            Log.d("SparkerDebug", "Error creating user profile");
                            hideProgressHUD();
                            ((MainActivity) getActivity()).logout();
                            return;
                        }
                    });
                }
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("SparkerDebug", "Error trying to retrieve user profile");
                hideProgressHUD();
                ((MainActivity) getActivity()).logout();
                return;
            }
        });
    }

    public void refreshCurrentUser() {

        ChatService.getInstance().getDialogs();

        if (AccessToken.getCurrentAccessToken() == null) {
            Log.d("SparkerDebug", "No FB Access token, logging out!");
            hideProgressHUD();
            ((MainActivity) getActivity()).logout();
        }
        AccessToken.refreshCurrentAccessTokenAsync(new AccessToken.AccessTokenRefreshCallback() {
            @Override
            public void OnTokenRefreshed(AccessToken accessToken) {
                Log.d("SparkerDebug", "Successfully refreshed access token");
                if (AccessToken.getCurrentAccessToken().getPermissions().contains("public_profile")) {
                    Log.d("SparkerDebug", "Requesting FB public profile");
                    GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            if (response.getError() != null) {
                                Log.d("SparkerDebug", "Error getting FB public profile, logging out");
                                Log.d("SparkerDebug", response.getError().getErrorMessage());
                                hideProgressHUD();
                                ((MainActivity) getActivity()).logout();
                                return;
                            }
                            Log.d("SparkerDebug", "Successfully got FB public profile");
                            if (object.has("birthday")) {
                                SparkerSingleton.getInstance().currentUserProfile.setAge(18);
                            } else {
                                SparkerSingleton.getInstance().currentUserProfile.setAge(18);
                            }
                            try {
                                SparkerSingleton.getInstance().currentUserProfile.setName(object.getString("first_name"));
                            } catch (Exception e) {

                            }

                            SparkerSingleton.getInstance().uploadCurrentUserProfile(new OnProfileUploadComplete() {
                                @Override
                                public void onProfileUploadComplete(Boolean error) {
                                    hideProgressHUD();
                                    if (error) {
                                        ((MainActivity) getActivity()).logout();
                                    } else {
                                        SparkerSingleton.getInstance().rosterSetup();
                                        startUpdatingLocation();
                                    }
                                }
                            });
                        }
                    });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "first_name,birthday");
                    request.setParameters(parameters);
                    request.executeAsync();
                }
            }

            @Override
            public void OnTokenRefreshFailed(FacebookException exception) {
                Log.d("SparkerDebug", "Error refreshing FB token");
                Log.d("SparkerDebug", exception.getMessage());
                hideProgressHUD();
                ((MainActivity) getActivity()).logout();
            }
        });
    }

    private void hideProgressHUD() {
        if (loginProgressHUD != null) {
            loginProgressHUD.dismiss();
        }
    }

    private void startUpdatingLocation() {

        Log.d("SparkerDebug", "Starting Updating Location");

        if (((MainActivity) getActivity()).mGoogleApiClient.isConnected()) {
            foundLocation = false;
            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        ((MainActivity) getActivity()).mGoogleApiClient, mLocationRequest, this);
            } catch (SecurityException e) {
                Log.d("SparkerDebug", "Permissions error get location updates");
            } catch (Exception e) {
                Log.d("SparkerDebug", "Error getting location updates");
            }
        } else {
            Log.d("SparkerDebug", "Google API Client not connected!");
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        foundLocation = true;
        LocationServices.FusedLocationApi.removeLocationUpdates(
                ((MainActivity) getActivity()).mGoogleApiClient, this);
        if (SparkerSingleton.getInstance().currentUserProfile != null) {
            SparkerSingleton.getInstance().currentUserProfile.setLocation(location);
            SparkerSingleton.getInstance().uploadCurrentUserProfile();
            SparkerSingleton.getInstance().loadUsers();
        }
    }

    public void settingsChanged() {
        SparkerSingleton.getInstance().resetCards();
    }

    private void didTapProfile() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putBoolean("cards", true);
        fragment.setArguments(args);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.layout, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void showOwnProfile() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putBoolean("own_profile", true);
        fragment.setArguments(args);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.layout, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void showMatchScreen(final int userID) {
        SparkerSingleton.getInstance().currentUserProfile.matched.add(userID + "");
        SparkerSingleton.getInstance().uploadCurrentUserProfile();
        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
        requestBuilder.eq("user_id", userID);
        QBCustomObjects.getObjects("profile", requestBuilder).performAsync(new QBEntityCallback<ArrayList<QBCustomObject>>() {
            @Override
            public void onSuccess(ArrayList<QBCustomObject> qbCustomObjects, Bundle bundle) {
                if (qbCustomObjects.size() > 0) {
                    final UserProfile profile = new UserProfile(qbCustomObjects.get(0));
                    QBChatDialog dialog = new QBChatDialog();
                    dialog.setType(QBDialogType.PRIVATE);
                    dialog.setOccupantsIds(Arrays.asList(userID, SparkerSingleton.getInstance().currentUserProfile.userID));
                    QBRestChatService.createChatDialog(dialog).performAsync(new QBEntityCallback<QBChatDialog>() {
                        @Override
                        public void onSuccess(QBChatDialog qbChatDialog, Bundle bundle) {
                            ChatService.getInstance().insertOrUpdateDialog(qbChatDialog);

                            if (isVisible()) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        MatchedFragment matchFragment = new MatchedFragment();
                                        Bundle bundle = new Bundle();
                                        bundle.putParcelable("profile", profile);
                                        matchFragment.setArguments(bundle);
                                        FragmentManager fm = getFragmentManager();
                                        matchFragment.show(fm, "fragment_matched");
                                    }
                                });
                            } else {
                                SparkerSingleton.getInstance().playNotification();
                                ChatService.getInstance().displayMessageNotification(true);
                            }
                        }

                        @Override
                        public void onError(QBResponseException e) {
                            Log.d("SparkerDebug", "Error creating dialog!");
                        }
                    });
                }
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("SparkerDebug", "Error trying to retrieve user profile");
            }
        });
    }

    public class SwipeDeckAdapter extends BaseAdapter {

        private ArrayList<UserProfile> data;
        private Context context;

        public SwipeDeckAdapter(ArrayList<UserProfile> users, Context context) {
            this.data = users;
            this.context = context;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;
            if (v == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                // normally use a viewholder
                v = inflater.inflate(R.layout.card_view, parent, false);
            }

            SquareRoundedImageView squareRoundedImageView = v.findViewById(R.id.imageView);
            squareRoundedImageView.setImageResource(R.drawable.placeholder);

            String nameAgeText = "";
            if (data.get(position).name != null) {
                nameAgeText = data.get(position).name + ", ";
            }
            nameAgeText += data.get(position).age;

            TextView nameTV = v.findViewById(R.id.nameTextView);
            nameTV.setText(nameAgeText);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    didTapProfile();
                }
            });

            return v;
        }
    }
}
