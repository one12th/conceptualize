package com.likeableincorporation.conceptualize;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.likeableincorporation.conceptualize.repository.model.UserProfile;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBIncomingMessagesManager;
import com.quickblox.chat.QBPrivateChat;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBChatDialogMessageListener;
import com.quickblox.chat.listeners.QBMessageListener;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by SamHarrison on 01/11/2016.
 */
public class ChatService implements ConnectionListener, QBChatDialogMessageListener {

    private static ChatService instance;
    public ArrayList<QBChatDialog> dialogs = new ArrayList<QBChatDialog>();
    public ArrayList<UserProfile> userProfiles = new ArrayList<UserProfile>();
    private ArrayList<QBChatDialog> sortedDialogs;
    //public ChatServiceDelegate
    private ArrayList<QBChatMessage> messages = new ArrayList<QBChatMessage>();
    private ArrayList<ChatListener> chatListeners = new ArrayList<ChatListener>();
    private QBMessageListener<QBPrivateChat> messageListener;

    public static ChatService getInstance() {
        if (instance == null) {
            instance = new ChatService();
        }
        return instance;
    }

    public void addChatListener(ChatListener listener) {
        if (!chatListeners.contains(listener)) {
            chatListeners.add(listener);
        }
    }

    public void removeChatListener(ChatListener listener) {
        if (chatListeners.contains(listener)) {
            chatListeners.remove(listener);
        }
    }

    /*public void setDialogs(ArrayList<QBChatDialog> dialogs) {
        Collections.sort(dialogs, new Comparator<QBChatDialog>() {
            @Override
            public int compare(QBChatDialog lhs, QBChatDialog rhs) {
                if(lhs.getLastMessageDateSent() == rhs.getLastMessageDateSent()){
                    return lhs.getCreatedAt().compareTo(rhs.getCreatedAt());
                }
                return new Date(lhs.getLastMessageDateSent()).compareTo(new Date(rhs.getLastMessageDateSent()));
            }
        });
        this.dialogs = dialogs;
    }*/

    public void reInitDialogs() {
        for (QBChatDialog dialog : dialogs) {
            dialog.initForChat(QBChatService.getInstance());
        }
    }

    private void sortDialogs() {
        Collections.sort(dialogs, new Comparator<QBChatDialog>() {
            @Override
            public int compare(QBChatDialog lhs, QBChatDialog rhs) {
                //if(rhs.getLastMessageDateSent() == lhs.getLastMessageDateSent()){
                return rhs.getUpdatedAt().compareTo(lhs.getUpdatedAt());
                //}
                //return new Date(rhs.getLastMessageDateSent()).compareTo(new Date(lhs.getLastMessageDateSent()));
            }
        });
    }

    public void getDialogs() {
        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
        requestBuilder.setLimit(100);
        QBRestChatService.getChatDialogs(null, requestBuilder).performAsync(new QBEntityCallback<ArrayList<QBChatDialog>>() {
            @Override
            public void onSuccess(ArrayList<QBChatDialog> qbChatDialogs, Bundle bundle) {
                if (qbChatDialogs != null) {
                    for (QBChatDialog dialog : qbChatDialogs) {
                        if (dialog.getLastMessage() != null && dialog.getLastMessage().equals("[user_deleted_dialog]")) {
                            continue;
                        } else if (dialog.getOccupants().size() <= 1 || dialog.getRecipientId() == null) {
                            deleteDialog(dialog);
                            continue;
                        } else {
                            insertOrUpdateDialog(dialog);
                        }
                    }
                }
            }

            @Override
            public void onError(QBResponseException e) {

            }
        });

    }

    public void downloadUserProfile(int userID) {

        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
        requestBuilder.eq("user_id", userID);
        QBCustomObjects.getObjects("profile", requestBuilder).performAsync(new QBEntityCallback<ArrayList<QBCustomObject>>() {
            @Override
            public void onSuccess(ArrayList<QBCustomObject> qbCustomObjects, Bundle bundle) {
                if (qbCustomObjects != null && qbCustomObjects.size() > 0) {
                    userProfiles.add(new UserProfile(qbCustomObjects.get(0)));
                }
            }

            @Override
            public void onError(QBResponseException e) {

            }
        });
    }

    public void insertOrUpdateDialog(QBChatDialog newDialog) {
        Intent intent = new Intent("dialogs-updated");
        for (QBChatDialog dialog : dialogs) {
            if (dialog.getDialogId().equals(newDialog.getDialogId())) {
                dialogs.set(dialogs.indexOf(dialog), newDialog);
                sortDialogs();
                LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(intent);
                return;
            }
        }
        dialogs.add(newDialog);
        sortDialogs();
        int opponentID = getDialogOpponentID(newDialog);
        if (opponentID != 0 && getUserProfileByID(opponentID) == null) {
            downloadUserProfile(opponentID);
        }
        LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(intent);
    }

    public QBChatDialog getDialogByOpponentID(int userID) {
        for (QBChatDialog dialog : dialogs) {
            if (dialog.getOccupants().contains(userID)) {
                return dialog;
            }
        }
        return null;
    }

    public int getDialogOpponentID(QBChatDialog dialog) {
        for (int occupant : dialog.getOccupants()) {
            if (occupant != SparkerSingleton.getInstance().currentUserProfile.userID) {
                return occupant;
            }
        }
        return 0;
    }

    public UserProfile getDialogOpponent(QBChatDialog dialog) {
        int opponentID = getDialogOpponentID(dialog);
        if (opponentID != 0) {
            return getUserProfileByID(opponentID);
        }
        return null;
    }

    public QBChatDialog getDialogByID(String dialogID) {
        for (QBChatDialog dialog : dialogs) {
            if (dialog.getDialogId().equals(dialogID)) {
                return dialog;
            }
        }
        return null;
    }

    public UserProfile getUserProfileByID(int userID) {
        for (UserProfile profile : userProfiles) {
            if (profile.userID == userID) {
                return profile;
            }
        }
        return null;
    }

    public ArrayList<QBChatMessage> messagesForDialogID(String dialogID) {
        ArrayList<QBChatMessage> curMessages = new ArrayList<QBChatMessage>();
        for (QBChatMessage message : messages) {
            if (message.getDialogId().equals(dialogID)) {
                curMessages.add(message);
            }
        }
        Collections.sort(curMessages, new Comparator<QBChatMessage>() {
            @Override
            public int compare(QBChatMessage lhs, QBChatMessage rhs) {
                return new Date(lhs.getDateSent()).compareTo(new Date(rhs.getDateSent()));
            }
        });
        return curMessages;
    }

    public void addMessages(ArrayList<QBChatMessage> messages) {
        for (QBChatMessage message : messages) {
            int index = this.messages.indexOf(message);
            if (index != -1) {
                this.messages.set(index, message);
            } else {
                this.messages.add(message);
            }
        }
    }

    public void sendDialogDeleteMessage(QBChatDialog dialog) {
        if (dialog == null) {
            return;
        }
        try {
            QBChatMessage chatMessage = new QBChatMessage();
            chatMessage.setMarkable(true);
            chatMessage.setBody("[user_deleted_dialog]");
            chatMessage.setProperty("save_to_history", "1");
            chatMessage.setProperty("delete_dialog", "1");
            dialog.sendMessage(chatMessage);
            if (dialogs.contains(dialog)) {
                dialogs.remove(dialog);
            }
        } catch (SmackException.NotConnectedException e) {
            Log.d("SparkerDebug", "Error sending delete dialog message!");
            if (dialogs.contains(dialog)) {
                dialogs.remove(dialog);
            }
        }
    }

    public void deleteDialog(final QBChatDialog dialog) {
        sendDialogDeleteMessage(dialog);
        int opponentID = getDialogOpponentID(dialog);
        if (opponentID != 0) {
            SparkerSingleton.getInstance().blockUser(opponentID);
        }
        QBRestChatService.deleteDialog(dialog.getDialogId(), false).performAsync(new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                Log.d("SparkerDebug", "Successfully delete dialog!");
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("SparkerDebug", "Error deleting dialog!");
            }
        });
    }

    //ADD ERROR CALLBACK
    public void sendMessage(String messageText, QBChatDialog dialog) {
        QBChatMessage chatMessage = new QBChatMessage();
        chatMessage.setMarkable(true);
        chatMessage.setBody(messageText);
        chatMessage.setProperty("save_to_history", "1");
        chatMessage.setSenderId(QBChatService.getInstance().getUser().getId());
        chatMessage.setDialogId(dialog.getDialogId());

        long time = System.currentTimeMillis() / 1000;
        chatMessage.setDateSent(time);

        try {
            dialog.sendMessage(chatMessage);
            processMessage(dialog.getDialogId(), chatMessage, chatMessage.getSenderId());
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }

    //QBChatDialogMessageListener
    @Override
    public void processMessage(String s, QBChatMessage qbChatMessage, Integer integer) {
        if (qbChatMessage.getProperties() != null && qbChatMessage.getProperty("delete_dialog") != null
                && qbChatMessage.getSenderId() != SparkerSingleton.getInstance().currentUserProfile.userID) {
            deleteDialog(getDialogByID(qbChatMessage.getDialogId()));
            return;
        }
        ArrayList<QBChatMessage> messages = new ArrayList<QBChatMessage>();
        messages.add(qbChatMessage);
        addMessages(messages);

        if (qbChatMessage.getSenderId() != null && qbChatMessage.getSenderId() != SparkerSingleton.getInstance().currentUserProfile.userID) {
            SparkerSingleton.getInstance().playNotification();
        }

        /*Intent intent = new Intent("new-message-received");
        intent.putExtra("dialogID",qbChatMessage.getDialogId());
        LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(intent);*/
        if (chatListeners.size() == 0) {
            displayMessageNotification(false);
        } else {
            for (ChatListener listener : chatListeners) {
                listener.messageReceived(qbChatMessage.getDialogId());
            }
        }

        QBChatDialog dialog = getDialogByID(qbChatMessage.getDialogId());
        if (dialog != null) {
            dialog.setUpdatedAt(new Date());
            dialog.setLastMessageUserId(qbChatMessage.getSenderId());
            dialog.setLastMessage(qbChatMessage.getBody());
            dialog.setLastMessageDateSent(qbChatMessage.getDateSent());
            int unreadCount = (dialog.getUnreadMessageCount() != null ? dialog.getUnreadMessageCount() : 0);
            dialog.setUnreadMessageCount(unreadCount + 1);
            insertOrUpdateDialog(dialog);
        } else {
            QBRestChatService.getChatDialogById(qbChatMessage.getDialogId()).performAsync(new QBEntityCallback<QBChatDialog>() {
                @Override
                public void onSuccess(QBChatDialog qbChatDialog, Bundle bundle) {
                    if (qbChatDialog != null) {
                        insertOrUpdateDialog(qbChatDialog);
                    }
                }

                @Override
                public void onError(QBResponseException e) {

                }
            });
        }
    }

    @Override
    public void processError(String s, QBChatException e, QBChatMessage qbChatMessage, Integer integer) {

    }

    public boolean shouldShowUnreadIndicator() {
        Date lastViewedDate = new Date(2017, 2, 8);
        if (SparkerSingleton.getInstance().currentUserProfile == null) {
            return false;
        }
        for (QBChatDialog dialog : dialogs) {
            if ((dialog.getUnreadMessageCount() != null && dialog.getUnreadMessageCount() > 0 && dialog.getLastMessageUserId() != null && dialog.getLastMessageUserId() != SparkerSingleton.getInstance().currentUserProfile.userID)
                    || (dialog.getLastMessageDateSent() == 0 && dialog.getCreatedAt().after(lastViewedDate))) {
                return true;
            }
        }
        return false;
    }

    //ConnectionListener
    @Override
    public void connected(XMPPConnection connection) {
        Log.d("SparkerDebug", "Chat connected");
        for (ChatListener listener : chatListeners) {
            listener.chatConnected();
        }
        QBIncomingMessagesManager messagesManager = QBChatService.getInstance().getIncomingMessagesManager();
        if (messagesManager != null) {
            messagesManager.addDialogMessageListener(this);
        }
    }

    public void displayMessageNotification(boolean newMatch) {
        /*int statusBarHeight = (int) Math.ceil(25 * MyApplication.getAppContext().getResources().getDisplayMetrics().density);

        View statusBarView = new View(MyApplication.getAppContext());
        statusBarView.setBackgroundColor(Color.GREEN);


        WindowManager.LayoutParams params = null;
        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.FILL_PARENT,
                statusBarHeight,
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                PixelFormat.TRANSLUCENT
        );

        params.gravity = Gravity.RIGHT | Gravity.TOP;
        WindowManager wm = (WindowManager) MyApplication.getAppContext().getSystemService(WINDOW_SERVICE);
        wm.addView(statusBarView, params);*/

        String title = newMatch ? "You Have a New Match!" : "New Message Received";

        final NotificationManager mNotificationManager =
                (NotificationManager) MyApplication.getAppContext().getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(MyApplication.getAppContext())
                        .setSmallIcon(R.drawable.icon_spark)
                        .setContentTitle(title)
                        .setContentText(title)
                        .setTicker(title);

        mNotificationManager.notify(1, mBuilder.build());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mNotificationManager.cancel(1);
            }
        }, 2000);
    }

    @Override
    public void authenticated(XMPPConnection xmppConnection, boolean b) {

    }

    @Override
    public void connectionClosed() {
        Log.d("SparkerDebug", "Chat connection closed");
        for (ChatListener listener : chatListeners) {
            listener.chatDisconnected();
        }
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        // connection closed on error. It will be established soon
        Log.d("SparkerDebug", "Chat connection closed with error");
        for (ChatListener listener : chatListeners) {
            listener.chatDisconnected();
        }
    }

    @Override
    public void reconnectingIn(int seconds) {
        Log.d("SparkerDebug", "Chat will reconnect");
    }

    @Override
    public void reconnectionSuccessful() {
        Log.d("SparkerDebug", "Chat reconnected successfully");
    }

    @Override
    public void reconnectionFailed(Exception e) {
        Log.d("SparkerDebug", "Chat failed to reconnect");
    }
}