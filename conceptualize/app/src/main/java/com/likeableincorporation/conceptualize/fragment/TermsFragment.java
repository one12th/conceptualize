package com.likeableincorporation.conceptualize.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.likeableincorporation.conceptualize.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class TermsFragment extends Fragment {


    public TermsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_terms, container, false);

        TextView termsTitle = (TextView) view.findViewById(R.id.terms_title);
        WebView webView = (WebView) view.findViewById(R.id.web_view);
        ImageButton backButton = (ImageButton) view.findViewById(R.id.back_button);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });


        Bundle args = getArguments();
        if (args.getBoolean("isTerms")) {
            termsTitle.setText("Terms of Use");
            webView.loadUrl("file:///android_asset/terms_of_use.html");
        } else {
            termsTitle.setText("Privacy Policy");
            webView.loadUrl("file:///android_asset/privacy_policy.html");
        }

        return view;
    }

}
