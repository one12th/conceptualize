package com.likeableincorporation.conceptualize.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.likeableincorporation.conceptualize.R;
import com.likeableincorporation.conceptualize.SparkerSingleton;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import fr.tvbarthel.lib.blurdialogfragment.SupportBlurDialogFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class LikeLimitDialogFragment extends SupportBlurDialogFragment {

    TextView likeLimitTimerLabel;

    public LikeLimitDialogFragment() { }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_like_limit, container, false);

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        likeLimitTimerLabel = (TextView) view.findViewById(R.id.like_limit_timer);

        Date now = new Date();
        new CountDownTimer(Math.max(5000, SparkerSingleton.getInstance().currentUserProfile.likeLimitLifted.getTime() - now.getTime()), 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                likeLimitTimerLabel.setText(String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1)));
            }

            @Override
            public void onFinish() {
                dismiss();
            }
        }.start();

//        Button button = (Button) view.findViewById(R.id.upgradeButton);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                UpgradeDialogFragment upgradeDialog = new UpgradeDialogFragment();
//                upgradeDialog.show(fm, "fragment_upgrade_dialog");
//                LikeLimitDialogFragment.this.dismiss();
//            }
//        });

        return view;
    }

    @Override
    protected boolean isDimmingEnable() {
        return true;
    }
}
